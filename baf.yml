name: tr181-pcp

short_description: "Port control protocol"
long_description: ""

type: plugin
category: amx

urls:
  opensource: "https://gitlab.com/prpl-foundation/components/core/plugins/tr181-pcp"

licenses:
- name: "BSD-2-Clause-Patent"

configs:
  CONFIG_SAH_AMX_TR181_PCP:
    super: True
    default: y
    type: bool
    description: "Build tr181 pcp plug-in"
  CONFIG_SAH_AMX_TR181_PCP_ORDER:
    depends: CONFIG_SAH_AMX_TR181_PCP
    default: 70
    type: int
    description: "Start up order for the tr181 pcp plugin"
  CONFIG_SAH_AMX_TR181_PCP_CARES_RPATH:
    super: False
    default: n
    type: bool
    description: "Enable linking and setting rpath to /opt/prplos"
  CONFIG_SAH_AMX_TR181_PCP_RUN_AS_USER:
    depends: CONFIG_SAH_AMX_TR181_PCP
    default: "tr181_app"
    type: string
    description: "User will be used for the t181-pcp plugin"
  CONFIG_SAH_AMX_TR181_PCP_RUN_AS_GROUP:
    depends: CONFIG_SAH_AMX_TR181_PCP
    default: "tr181_app"
    type: string
    description: "Group will be used for the t181-pcp plugin"


makefiles:
  compile:
    - directory: "src"
    - directory: "odl"
    - directory: "mod-pcp-libpcp/src"
  clean:
    - directory: "src"
    - directory: "odl"
    - directory: "test"
    - directory: "mod-pcp-libpcp/src"
  test:
    - directory: "test"
      target: "run"
    - directory: "test"
      target: "coverage"

artifacts:
- source: "odl/$(COMPONENT).odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT).odl"
  permissions: "0644"
- source: "odl/$(COMPONENT)_definition.odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl"
  permissions: "0644"
- source: "odl/$(COMPONENT)_client.odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT)_client.odl"
  permissions: "0644"
- source: "odl/$(COMPONENT)_server.odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT)_server.odl"
  permissions: "0644"
- source: "odl/$(COMPONENT)_inboundmapping.odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT)_inboundmapping.odl"
  permissions: "0644"
- source: "odl/$(COMPONENT)_outboundmapping.odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT)_outboundmapping.odl"
  permissions: "0644"
- source: "odl/$(COMPONENT)_caps.odl"
  destination: "/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl"
  permissions: "0644"
- source: "odl/defaults.d/*.odl"
  destination: "/etc/amx/$(COMPONENT)/defaults.d/"
  permissions: "0644"
- source: "odl/$(COMPONENT)_mapping.odl"
  destination: "/etc/amx/tr181-device/extensions/01_device-pcp_mapping.odl"
  permissions: "0644"

- source: "output/$(MACHINE)/object/$(COMPONENT).so"
  destination: "/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so"
  permissions: "0644"
  version: False

- source: "output/$(MACHINE)/mod-pcp-libpcp.so"
  destination: "/usr/lib/amx/$(COMPONENT)/modules/mod-pcp-libpcp.so"
  permissions: "0644"
  version: False

- source: "$(BINDIR)/amxrt"
  destination: "$(BINDIR)/$(COMPONENT)"
  symbolic: "True"

- source: "scripts/$(COMPONENT).sh"
  destination: "$(INITDIR)/$(COMPONENT)"
  permissions: "0755"
  debug:
    order: $(CONFIG_SAH_AMX_TR181_PCP_ORDER)
  init:
    start: $(CONFIG_SAH_AMX_TR181_PCP_ORDER)
    stop: $(CONFIG_SAH_AMX_TR181_PCP_ORDER)

dependencies:
  compile:
  - name: "libamxb"
    min: "*"
  - name: "libamxc"
    min: "*"
  - name: "libamxp"
    min: "*"
  - name: "libamxd"
    min: "*"
  - name: "libamxo"
    min: "*"
  - name: "libamxm"
    min: "*"
  - name: "libsahtrace"
    min: "*"
  - name: "libpcp"
    min: "*"
  - name: "libnetmodel"
    min: "*"
  - name: "libipat"
    min: "*" 
  - name: "c-ares"
    min: "gen_1.19.0_v1.0.0"

  runtime:
  - name: "libamxb"
  - name: "libamxc"
  - name: "libamxp"
  - name: "libamxd"
  - name: "libamxo"
  - name: "libamxm"
  - name: "libsahtrace"
  - name: "libipat"
  - name: "amxrt"
  - name: "libpcp"
  - name: "libnetmodel"
  - name: "c-ares"

doc:
  parser: "amx"
  odl:
    - "odl/$(COMPONENT)_definition.odl"
