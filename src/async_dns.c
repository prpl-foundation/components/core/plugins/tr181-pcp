/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ares.h>
#include <arpa/inet.h>

#include "pcp.h"

#define ME "pcp"

static void let_cares_handle_timeouts(UNUSED amxp_timer_t* timer, void* priv) {
    server_t* server = (server_t*) priv;

    SAH_TRACEZ_INFO(ME, "Timer[getaddrinfo] expired");

    ares_process_fd(server->async_dns.channel, ARES_SOCKET_BAD, ARES_SOCKET_BAD);
}

static void handle_addrinfo_result(void* arg, int status, UNUSED int timeouts, struct ares_addrinfo* result) {
    server_t* server = (server_t*) arg;

    SAH_TRACEZ_INFO(ME, "Server[%s] status: %s", name(server->object), ares_strerror(status));

    amxp_timer_stop(server->async_dns.getaddrinfo);

    when_true((status == ARES_EDESTRUCTION) || (status == ARES_ECANCELLED), exit);
    if(status != ARES_SUCCESS) {
        SAH_TRACEZ_INFO(ME, "Server[%s] DNS lookup failed: %s", name(server->object), ares_strerror(status));
        set_status(server->object, ERROR);
        goto exit;
    }

    // only stop this timer when result is ARES_SUCCESS
    amxp_timer_stop(server->async_dns.retransmission);

    amxc_var_delete(&server->async_dns.addrinfo);
    amxc_var_new(&server->async_dns.addrinfo);
    amxc_var_set_type(server->async_dns.addrinfo, AMXC_VAR_ID_LIST);

    for(struct ares_addrinfo_node* ai = result->nodes; ai != NULL; ai = ai->ai_next) {
        amxc_var_t* var = NULL;
        char buffer[INET6_ADDRSTRLEN] = {'\0'};
        const char* pbuffer = NULL;
        int family = ai->ai_family == AF_INET ? 4 : 6;

        if(family == 4) {
            struct sockaddr_in* sa = (struct sockaddr_in*) ai->ai_addr;
            pbuffer = inet_ntop(AF_INET, &sa->sin_addr, buffer, sizeof(buffer));
            SAH_TRACEZ_INFO(ME, "skip ipv4[%s]", pbuffer);
            continue;
        } else {
            struct sockaddr_in6* sa = (struct sockaddr_in6*) ai->ai_addr;
            pbuffer = inet_ntop(AF_INET6, &sa->sin6_addr, buffer, sizeof(buffer));
            SAH_TRACEZ_INFO(ME, "use ipv6[%s]", pbuffer);
        }
        if(pbuffer == NULL) {
            SAH_TRACEZ_WARNING(ME, "Failed to convert ipv%d address to string", family);
            continue;
        }

        var = amxc_var_add(amxc_htable_t, server->async_dns.addrinfo, NULL);
        amxc_var_add_key(cstring_t, var, "address", buffer);
        amxc_var_add_key(int32_t, var, "ttl", ai->ai_ttl);
        amxc_var_add_key(int32_t, var, "family", family);
    }

    nslookup_cb(server);

exit:
    ares_freeaddrinfo(result);
    return;
}

static void start_getaddrinfo(server_t* server) {
    int rv = ARES_ENOMEM;
    uint32_t timeout = 0;

    timeout = amxc_var_dyncast(uint32_t, server->async_dns.interval);
    when_true_trace(timeout == 0, out, ERROR, "Stop DNS lookup of '%s'", server->domain_name);

    rv = ares_init_options(&server->async_dns.channel,
                           &server->async_dns.options,
                           server->async_dns.optmask);
    when_false_trace(rv == ARES_SUCCESS, out, ERROR, "ares_init_options failed");
    ares_getaddrinfo(server->async_dns.channel, server->domain_name, NULL, &server->async_dns.hints, handle_addrinfo_result, server);

    SAH_TRACEZ_INFO(ME, "Start timer[retransmission] %ums", timeout);
    amxp_timer_start(server->async_dns.retransmission, timeout);

out:
    return;
}

static void cancel_getaddrinfo(server_t* server) {
    if(server->async_dns.channel != NULL) {
        ares_destroy(server->async_dns.channel);
        server->async_dns.channel = NULL;
    }
    if(server->async_dns.fds != NULL) {
        amxc_var_for_each(var, server->async_dns.fds) {
            int fd = GET_INT32(var, NULL);
            SAH_TRACEZ_INFO(ME, "Remove fd[%d]", fd);
            amxo_connection_remove(get_parser(), fd);
        }
        amxc_var_delete(&server->async_dns.fds);
    }
    if(server->async_dns.addrinfo != NULL) {
        amxc_var_delete(&server->async_dns.addrinfo);
    }
}

static void let_cares_handle_socket_event(int fd, void* data) {
    server_t* server = (server_t*) data;

    SAH_TRACEZ_INFO(ME, "Process fd[%d]", fd);

    ares_process_fd(server->async_dns.channel, fd, ARES_SOCKET_BAD);
}

static void pcp_ares_sock_cb(void* data, ares_socket_t socket_fd, int readable, int writable) {
    server_t* server = (server_t*) data;
    uint32_t timeout = 0;
    struct timeval tv = {0, 0};

    SAH_TRACEZ_INFO(ME, "Fd[%d] R %d W %d", socket_fd, readable, writable);

    when_false((readable + writable) > 0, exit);

    if(server->async_dns.fds == NULL) {
        amxc_var_new(&server->async_dns.fds);
        amxc_var_set_type(server->async_dns.fds, AMXC_VAR_ID_LIST);
    }

    SAH_TRACEZ_INFO(ME, "Add fd[%d]", socket_fd);
    amxc_var_add(int32_t, server->async_dns.fds, socket_fd);
    amxo_connection_add(get_parser(), socket_fd, let_cares_handle_socket_event, NULL, AMXO_CUSTOM, server);

    ares_timeout(server->async_dns.channel, NULL, &tv);
    timeout = (tv.tv_sec * (uint32_t) 1000) + (tv.tv_usec / 1000);
    SAH_TRACEZ_INFO(ME, "Start timer[getaddrinfo] %ums", timeout);
    amxp_timer_start(server->async_dns.getaddrinfo, timeout);

exit:
    return;
}

static void restart_getaddrinfo(UNUSED amxp_timer_t* timer, void* priv) {
    server_t* server = (server_t*) priv;

    SAH_TRACEZ_INFO(ME, "Timer[retransmission] expired");

    amxp_timer_stop(server->async_dns.getaddrinfo);
    cancel_getaddrinfo(server);

    server->async_dns.interval = amxc_var_get_next(server->async_dns.interval);
    when_null_trace(server->async_dns.interval, out, ERROR, "Stop DNS lookup of '%s'", server->domain_name);

    start_getaddrinfo(server);

out:
    return;
}

void nslookup(server_t* server, const char* domain_name) {
    when_str_empty(domain_name, exit);

    free(server->domain_name);
    server->domain_name = strdup(domain_name);
    when_null(server->domain_name, exit);

    if(server->async_dns.getaddrinfo == NULL) {
        amxp_timer_new(&server->async_dns.getaddrinfo, let_cares_handle_timeouts, server);
    }

    if(server->async_dns.interval == NULL) {
        amxc_var_init(&server->async_dns.intervals);

        when_failed_trace(amxd_object_get_param(server->object, "RetransmissionStrategy", &server->async_dns.intervals), exit, ERROR, "Failed to get parameter RetransmissionStrategy");

        amxc_var_cast(&server->async_dns.intervals, AMXC_VAR_ID_LIST);

        server->async_dns.interval = amxc_var_get_first(&server->async_dns.intervals);

        if(server->async_dns.retransmission == NULL) {
            amxp_timer_new(&server->async_dns.retransmission, restart_getaddrinfo, server);
        }
    }

    server->async_dns.hints.ai_family = AF_INET6;
    server->async_dns.hints.ai_socktype = SOCK_DGRAM;
    server->async_dns.hints.ai_flags = ARES_AI_NOSORT;

    server->async_dns.options.sock_state_cb = pcp_ares_sock_cb;
    server->async_dns.options.sock_state_cb_data = server;
    server->async_dns.options.tries = 1;
    server->async_dns.options.timeout = 300 * 1000; // 5 minutes
    server->async_dns.optmask = ARES_OPT_SOCK_STATE_CB | ARES_OPT_TIMEOUTMS | ARES_OPT_TRIES;

    start_getaddrinfo(server);

exit:
    return;
}

void nslookup_cancel(server_t* server) {
    when_null(server, exit);

    cancel_getaddrinfo(server);

    if(server->async_dns.getaddrinfo != NULL) {
        amxp_timer_delete(&server->async_dns.getaddrinfo);
    }
    if(server->async_dns.retransmission != NULL) {
        amxp_timer_delete(&server->async_dns.retransmission);
    }
    amxc_var_clean(&server->async_dns.intervals);
    server->async_dns.interval = NULL;
    FREE(server->domain_name);

exit:
    return;
}
