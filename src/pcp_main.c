/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <ares.h>

#include "pcp.h"
#include "dm_pcp.h"
#include <amxd/amxd_transaction.h>
#include <amxm/amxm.h>
#include <netmodel/client.h>

#define ME "pcp"

static amxd_dm_t* pcp_dm = NULL;
static amxo_parser_t* pcp_parser = NULL;
static amxc_var_t read_handlers;

amxd_dm_t* get_dm(void) {
    return pcp_dm;
}

amxo_parser_t* get_parser(void) {
    return pcp_parser;
}

static inline amxc_var_t* get_config(void) {
    return pcp_parser != NULL ? &pcp_parser->config : NULL;
}

static void load_mod_core(void) {
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_NOTICE(ME, "Load module[core]");

    // methods that can be called from modules
    amxm_module_register(&mod, NULL, "core");
    amxm_module_add_function(mod, "map-state-changed", inboundmapping_state_changed);
}

static bool load_mod_pcp(void) {
    amxm_shared_object_t* so = NULL;
    const char* path = GETP_CHAR(get_config(), "modules.mod-pcp");
    char* lifetime = NULL;
    int rv = -1;

    SAH_TRACEZ_NOTICE(ME, "Load module[%s]", path);

    rv = amxm_so_open(&so, "pcp", path);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load module[%s]", path);
        goto exit;
    }

exit:
    free(lifetime);
    return rv == 0;
}

static bool load_mod_firewall(void) {
    amxm_shared_object_t* so = NULL;
    const char* path = GETP_CHAR(get_config(), "modules.mod-firewall");
    int rv = -1;

    SAH_TRACEZ_NOTICE(ME, "Load module[%s]", path);

    rv = amxm_so_open(&so, "fw", path);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load module[%s]", path);
        goto exit;
    }

exit:
    return rv == 0;
}

static void get_values_from_mod_pcp(void) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_trans_t trans;
    amxd_status_t retval = amxd_status_unknown_error;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "PCP");

    rv = amxm_execute_function("pcp", "pcp", "supported-versions", &args, &ret);
    if(rv == 0) {
        amxd_trans_set_value(cstring_t, &trans, "SupportedVersions", GET_CHAR(&ret, NULL));
    }

    rv = amxm_execute_function("pcp", "pcp", "supported-options", &args, &ret);
    if(rv == 0) {
        amxd_trans_set_value(cstring_t, &trans, "OptionList", GET_CHAR(&ret, NULL));
    }

    retval = amxd_trans_apply(&trans, get_dm());
    when_false_trace(retval == amxd_status_ok, exit, ERROR, "Transaction failed");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxd_trans_clean(&trans);
}

static int init(amxd_dm_t* dm, amxo_parser_t* parser) {
    pcp_dm = dm;
    pcp_parser = parser;
    netmodel_initialize();
    ares_library_init(ARES_LIB_INIT_ALL);
    amxc_var_init(&read_handlers);
    amxc_var_set_type(&read_handlers, AMXC_VAR_ID_HTABLE);
    load_mod_core();
    load_mod_pcp();
    load_mod_firewall();
    get_values_from_mod_pcp();
    return 0;
}

static int cleanup(void) {
    amxm_close_all();
    amxc_var_clean(&read_handlers);
    ares_library_cleanup();
    netmodel_cleanup();
    pcp_dm = NULL;
    pcp_parser = NULL;
    return 0;
}

int _pcp_main(int reason,
              amxd_dm_t* dm,
              amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case 0:     // START
        retval = init(dm, parser);
        break;
    case 1:     // STOP
        retval = cleanup();
        break;
    }

    return retval;
}
