/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "pcp.h"
#include "dm_pcp.h"
#include <amxm/amxm.h>
#include <amxd/amxd_transaction.h>

#define ME "pcp"

const char* pcpstatus_to_str(pcpstatus_t pcpstatus) {
    const char* status = NULL;

    switch(pcpstatus) {
    case SUCCESS:
        status = "Enabled";
        break;
    case DISABLED:
        status = "Disabled";
        break;
    case STACKDISABLED:
        status = "StackDisabled";
        break;
    default:
        status = "Error";
    }

    return status;
}

void set_status(amxd_object_t* obj, pcpstatus_t status) {
    amxd_trans_t trans;

    when_true(status == LAST, exit);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", pcpstatus_to_str(status));
    amxd_trans_apply(&trans, get_dm());
    amxd_trans_clean(&trans);
exit:
    return;
}

static void init_maps(amxd_object_t* server_obj) {
    amxd_object_t* map_templ = amxd_object_findf(server_obj, "InboundMapping.");
    amxd_object_for_each(instance, it, map_templ) {
        amxd_object_t* map_obj = amxc_container_of(it, amxd_object_t, it);
        if(map_obj->priv == NULL) {
            new_inboundmapping(map_obj);
        }
    }
}

static void init_servers(amxd_object_t* client_obj) {
    amxd_object_t* server_templ = amxd_object_findf(client_obj, "Server.");
    amxd_object_for_each(instance, it, server_templ) {
        amxd_object_t* server_obj = amxc_container_of(it, amxd_object_t, it);
        init_maps(server_obj);
        if(server_obj->priv == NULL) {
            new_server(server_obj);
        }
    }
}

void _app_start(UNUSED const char* const sig_name,
                UNUSED const amxc_var_t* const data,
                UNUSED void* const priv) {
    amxd_object_t* client_templ = amxd_dm_findf(get_dm(), "PCP.Client.");
    amxd_object_for_each(instance, it, client_templ) {
        amxd_object_t* client_obj = amxc_container_of(it, amxd_object_t, it);
        init_servers(client_obj);
        if(client_obj->priv == NULL) {
            new_client(client_obj);
        }
    }
}

void _debug_changed(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set(bool, &args, GETP_BOOL(data, "parameters.Debug.to"));

    rv = amxm_execute_function("pcp", "pcp", "debug", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to toggle debug");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void _enable_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    all_clients_enable_changed(obj, GETP_BOOL(data, "parameters.0.to"));
}

bool stack_is_enabled(void) {
    amxd_object_t* obj = amxd_object_findf(amxd_dm_get_root(get_dm()), "PCP.");
    bool enable = false;

    enable = amxd_object_get_value(bool, obj, "Enable", NULL);

    return enable;
}
