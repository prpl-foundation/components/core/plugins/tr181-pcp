/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include "pcp.h"
#include "dm_pcp.h"
#include <amxd/amxd_transaction.h>
#include <amxm/amxm.h>
#include <netmodel/common_api.h>

#define ME "pcp"

#define PCP_SERVER_PORT 5351

bool server_is_ready(amxd_object_t* obj) {
    server_t* server = (server_t*) obj->priv;
    return server == NULL ? false : server->added;
}

static bool is_ipaddress(const char* address) {
    bool rv = false;
    struct sockaddr_storage sa;
    struct sockaddr_in* sa4 = (struct sockaddr_in*) &sa;
    struct sockaddr_in6* sa6 = (struct sockaddr_in6*) &sa;

    rv = 1 == inet_pton(AF_INET6, address, &sa6->sin6_addr);
    when_true(rv, exit);
    rv = 1 == inet_pton(AF_INET, address, &sa4->sin_addr);

exit:
    return rv;
}

static int add_dummy_mapping(amxd_object_t* server_obj) {
    amxd_trans_t trans;
    amxd_object_t* map_template = NULL;
    int rv = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    when_null_trace(server_obj, exit, ERROR, "Server object null");
    map_template = amxd_object_findf(server_obj, "InboundMapping.");

    amxd_trans_select_object(&trans, map_template);
    amxd_trans_add_inst(&trans, 0, DUMMY_NAME);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", 17);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 9);
    amxd_trans_set_value(uint32_t, &trans, "Lifetime", 5);
    amxd_trans_set_value(cstring_t, &trans, "ThirdPartyAddress", "192.168.1.1");

    rv = amxd_trans_apply(&trans, get_dm());
    when_failed_trace(rv, exit, ERROR, "Transaction failed: %d", rv);

exit:
    amxd_trans_clean(&trans);
    return rv;
}

static void add_server(server_t* server) {
    amxd_object_t* server_obj = server->object;
    amxd_object_t* client_obj = get_client_by_server(server_obj);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    uint32_t version = amxd_object_get_value(uint32_t, amxd_dm_findf(get_dm(), "PCP."), "PreferredVersion", NULL);
    const char* address = object_da_string(server_obj, "ServerAddressInUse");
    int rv = -1;

    SAH_TRACEZ_INFO(ME, "Enable server[%s]", name(server_obj));

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_true_trace(server->added, exit, INFO, "Server already added");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    var = amxc_var_add_key(amxc_htable_t, &args, "server", NULL);
    amxc_var_add_key(cstring_t, var, "id", name(server_obj));
    amxc_var_add_key(cstring_t, var, "client-id", name(client_obj));
    amxc_var_add_key(uint32_t, var, "version", version);
    amxc_var_add_key(cstring_t, var, "ipaddress", address);
    amxc_var_add_key(uint32_t, var, "port", PCP_SERVER_PORT);

    var = amxc_var_add_key(amxc_llist_t, &args, "map", NULL);
    amxd_object_for_all(server_obj, "InboundMapping.[Enable==true]", append_map, var);

    rv = amxm_execute_function("pcp", "pcp", "server-add", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to execute mod[pcp] func[server-add]");

    server->added = true;
    set_status(server->object, SUCCESS);

    rv = add_dummy_mapping(server_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to add dummy mapping to get external IP");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void parse_nameoraddress(server_t* server, const char* nameoraddress) {
    amxd_trans_t trans;
    int rv = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, server->object);

    nslookup_cancel(server);
    amxd_trans_set_value(cstring_t, &trans, "AdditionalServerAddresses", "");

    if(is_ipaddress(nameoraddress)) {
        amxd_trans_set_value(cstring_t, &trans, "ServerAddressInUse", nameoraddress);
        rv = amxd_trans_apply(&trans, get_dm());
        when_failed_trace(rv, exit, ERROR, "Transaction failed");
        add_server(server);
    } else {
        amxd_trans_set_value(cstring_t, &trans, "ServerAddressInUse", "");
        rv = amxd_trans_apply(&trans, get_dm());
        when_failed_trace(rv, exit, ERROR, "Transaction failed");
        nslookup(server, nameoraddress);
    }

exit:
    amxd_trans_clean(&trans);
}

static void remove_server(amxd_object_t* obj) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    amxd_trans_t trans;
    server_t* server = (server_t*) obj->priv;
    int rv = -1;

    when_null(server, exit);
    when_false_trace(server->added, exit, INFO, "No server to remove");

    SAH_TRACEZ_NOTICE(ME, "Disable server[%s]", name(obj));

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    var = amxc_var_add_key(amxc_htable_t, &args, "server", NULL);
    amxc_var_add_key(cstring_t, var, "id", name(obj));

    rv = amxm_execute_function("pcp", "pcp", "server-remove", &args, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute mod[pcp] func[server-remove]");
    }

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "ServerAddressInUse", "");
    amxd_object_for_all(obj, "InboundMapping.[Enable==true].", disable_map, NULL);
    amxd_trans_apply(&trans, get_dm());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxd_trans_clean(&trans);

exit:
    server->added = false;
}

void nslookup_cb(server_t* server) {
    amxd_trans_t trans;
    amxc_var_t* first = NULL;
    amxc_string_t csv_list;
    int rv = amxd_status_unknown_error;
    int count = 0;
    const char* additional_addresses = NULL;

    amxc_string_init(&csv_list, 0);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, server->object);

    amxc_var_for_each(var, server->async_dns.addrinfo) {
        const char* address = GET_CHAR(var, "address");
        if(first == NULL) {
            first = var;
            amxd_trans_set_value(cstring_t, &trans, "ServerAddressInUse", address);
            continue;
        }
        if(count == 0) {
            amxc_string_set(&csv_list, address);
        } else {
            amxc_string_appendf(&csv_list, ",%s", address);
        }
        count++;
    }
    additional_addresses = !string_empty(amxc_string_get(&csv_list, 0)) ? amxc_string_get(&csv_list, 0) : "";
    amxd_trans_set_value(cstring_t, &trans, "AdditionalServerAddresses", additional_addresses);
    rv = amxd_trans_apply(&trans, get_dm());
    when_false_trace(rv == amxd_status_ok, exit, WARNING, "Transaction failed");

    add_server(server);
exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&csv_list);
}

server_t* new_server(amxd_object_t* obj) {
    server_t* server = NULL;
    bool enable = false;
    const char* origin = NULL;

    server = (server_t*) calloc(1, sizeof(server_t));
    when_null(server, exit);

    origin = object_da_string(obj, "Origin");
    if((origin != NULL) && (strcmp(origin, "DHCPv6") == 0)) {
        server->origin = DHCPV6;
    } else {
        if(origin == NULL) {
            SAH_TRACEZ_ERROR(ME, "Origin should not be empty, default to Static");
        }
        server->origin = STATIC;
    }

    if(server->origin != STATIC) {
        amxd_param_t* param = amxd_object_get_param_def(obj, "ServerNameOrAddress");
        amxd_param_set_attr(param, amxd_pattr_persistent, false);
    }

    server->object = obj;
    obj->priv = server;

    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
    when_false(enable, exit);
    enable_server(NULL, obj, NULL);
exit:
    return server;
}

static void start_server(server_t* server) {
    const char* nameoraddress = NULL;
    pcpstatus_t status = ERROR;
    bool enable = amxd_object_get_value(bool, server->object, "Enable", NULL);

    when_false_trace(enable, skip_status, INFO, "Server is disabled");
    if(!client_is_ready(amxd_object_findf(server->object, ".^.^"))) {
        status = DISABLED;
        SAH_TRACEZ_INFO(ME, "Client is disabled");
        goto exit;
    }

    nameoraddress = object_da_string(server->object, "ServerNameOrAddress");
    if(string_empty(nameoraddress)) {
        if(server->origin != STATIC) {
            nameoraddress = client_get_nameoraddress(amxd_object_findf(server->object, ".^.^"));
            if(!string_empty(nameoraddress)) {
                amxd_trans_t trans;
                amxd_status_t trans_rv = amxd_status_unknown_error;
                amxd_trans_init(&trans);
                amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
                amxd_trans_select_object(&trans, server->object);
                amxd_trans_set_value(cstring_t, &trans, "ServerNameOrAddress", nameoraddress);
                trans_rv = amxd_trans_apply(&trans, get_dm());
                if(trans_rv != amxd_status_ok) {
                    SAH_TRACEZ_WARNING(ME, "Transaction failed");
                }
                amxd_trans_clean(&trans);
            }
        }
    }
    when_str_empty_trace(nameoraddress, exit, NOTICE, "ServerNameOrAddress is empty");
    parse_nameoraddress(server, nameoraddress);
    status = SUCCESS;
exit:
    set_status(server->object, status);
skip_status:
    return;
}

// either called by:
// - client to enable static server
// - new server
// - nameoraddress changed
// - server enable changed
int enable_server(UNUSED amxd_object_t* templ, amxd_object_t* obj, UNUSED void* priv) {
    server_t* server = (server_t*) obj->priv;

    when_null(server, exit);
    start_server(server);

exit:
    return 0;
}

static void stop_server(server_t* server) {
    amxd_trans_t trans;
    amxd_status_t rv = amxd_status_unknown_error;

    nslookup_cancel(server);
    remove_server(server->object);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, server->object);
    amxd_trans_set_value(cstring_t, &trans, "ServerAddressInUse", "");
    amxd_trans_set_value(cstring_t, &trans, "AdditionalServerAddresses", "");
    amxd_trans_set_value(cstring_t, &trans, "ExternalIPAddress", "");
    rv = amxd_trans_apply(&trans, get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
}

// either called by:
// - client
// - server enable changed
// - server removed
int disable_server(UNUSED amxd_object_t* templ, amxd_object_t* obj, UNUSED void* priv) {
    server_t* server = (server_t*) obj->priv;
    bool enable = false;

    when_null(server, exit);

    stop_server(server);

    if(server->origin != STATIC) {
        amxd_trans_t trans;
        amxd_status_t rv = amxd_status_unknown_error;
        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, obj);
        amxd_trans_set_value(cstring_t, &trans, "ServerNameOrAddress", "");
        rv = amxd_trans_apply(&trans, get_dm());
        if(rv != amxd_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Transaction failed");
        }
        amxd_trans_clean(&trans);
    }
exit:
    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
    set_status(obj, enable ? ERROR : DISABLED);
    return 0;
}

void _server_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* server_obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));

    new_server(server_obj);
}

void _server_enable_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    amxd_object_t* server_obj = amxd_dm_signal_get_object(get_dm(), data);
    bool is_enabling = false;

    is_enabling = GETP_BOOL(data, "parameters.Enable.to");

    if(is_enabling) {
        enable_server(NULL, server_obj, NULL);
    } else {
        disable_server(NULL, server_obj, NULL);
    }
}

void _server_nameoraddress_changed(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    server_t* server = (server_t*) obj->priv;
    bool name_changed = false;
    const char* nameoraddress = NULL;

    when_null(server, exit);

    nameoraddress = object_da_string(obj, "ServerNameOrAddress");
    name_changed = string_empty(server->domain_name) || (strcmp(nameoraddress, server->domain_name) != 0);
    when_false(name_changed, exit);
    SAH_TRACEZ_INFO(ME, "ServerNameOrAddress changed to '%s'", nameoraddress);
    stop_server(server);
    start_server(server);

exit:
    return;
}

typedef struct {
    amxd_trans_t trans;
    const char* nameoraddress;
} noa_t;

static int set_nameoraddress(UNUSED amxd_object_t* templ, amxd_object_t* obj, void* priv) {
    noa_t* noa = (noa_t*) priv;
    amxd_trans_select_object(&noa->trans, obj);
    amxd_trans_set_value(cstring_t, &noa->trans, "ServerNameOrAddress", noa->nameoraddress);
    return 0;
}

void all_servers_set_nameoraddress(amxd_object_t* client_obj, const char* nameoraddress) {
    noa_t noa;
    amxd_status_t rv = amxd_status_unknown_error;

    noa.nameoraddress = nameoraddress;
    amxd_trans_init(&noa.trans);
    amxd_trans_set_attr(&noa.trans, amxd_tattr_change_ro, true);
    amxd_object_for_all(client_obj, "Server.[Origin=='DHCPv6'].", set_nameoraddress, &noa);

    rv = amxd_trans_apply(&noa.trans, get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Transaction failed");
    }
    amxd_trans_clean(&noa.trans);
}

amxd_status_t _server_destroy(amxd_object_t* obj,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              UNUSED amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_true_status(amxd_object_get_type(obj) == amxd_object_template,
                     exit,
                     status = amxd_status_ok);

    disable_server(NULL, obj, NULL);
    FREE(obj->priv);

    status = amxd_status_ok;
exit:
    return status;
}