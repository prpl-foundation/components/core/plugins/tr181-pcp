/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ipat/ipat.h>

#include "pcp.h"
#include "dm_pcp.h"
#include <amxd/amxd_transaction.h>
#include <amxm/amxm.h>
#include <netmodel/common_api.h>

#define ME "pcp"

static int append_filter(UNUSED amxd_object_t* templ, amxd_object_t* instance, void* priv) {
    amxc_var_t* var = (amxc_var_t*) priv;
    amxc_var_t* subvar = NULL;
    const char* ipaddress = object_da_string(instance, "RemoteHostIPAddress");
    uint32_t prefix = amxd_object_get_value(uint32_t, instance, "PrefixLength", NULL);
    uint32_t port = amxd_object_get_value(uint32_t, instance, "RemotePort", NULL);

    subvar = amxc_var_add(amxc_htable_t, var, NULL);
    amxc_var_add_key(cstring_t, subvar, "ipaddress", ipaddress);
    amxc_var_add_key(uint32_t, subvar, "port", port);
    amxc_var_add_key(int32_t, subvar, "prefix", prefix);

    return 0;
}

void get_port_and_protocol(amxd_object_t* obj, int* port, int* protocol, int wildcard_value) {
    *port = amxd_object_get_value(int32_t, obj, "InternalPort", NULL);
    *protocol = amxd_object_get_value(int32_t, obj, "ProtocolNumber", NULL);

    if(*protocol <= 0) {
        *protocol = wildcard_value;
        *port = wildcard_value;
    }
}

int append_map(UNUSED amxd_object_t* templ, amxd_object_t* instance, void* priv) {
    amxc_var_t* var = (amxc_var_t*) priv;
    amxc_var_t* subvar = NULL;
    amxc_var_t* subsubvar = NULL;
    uint32_t lifetime = 0;
    int port = 0;
    int protocol = 0;
    const char* thirdparty_address = object_da_string(instance, "ThirdPartyAddress");
    const char* src_address = client_get_srcaddress(amxd_object_findf(instance, ".^.^.^.^"));

    get_port_and_protocol(instance, &port, &protocol, 0);
    lifetime = amxd_object_get_value(uint32_t, instance, "Lifetime", NULL);

    subvar = amxc_var_add(amxc_htable_t, var, NULL);
    amxc_var_add_key(cstring_t, subvar, "id", name(instance));
    amxc_var_add_key(cstring_t, subvar, "srcaddress", src_address);
    amxc_var_add_key(uint32_t, subvar, "port", port);
    amxc_var_add_key(int32_t, subvar, "protocol", protocol);
    amxc_var_add_key(uint32_t, subvar, "lifetime", lifetime);
    if(!string_empty(thirdparty_address)) {
        amxc_var_add_key(cstring_t, subvar, "thirdparty_address", thirdparty_address);
    }

    subsubvar = amxc_var_add_key(amxc_llist_t, subvar, "filter", NULL);
    amxd_object_for_all(instance, "Filter.*", append_filter, subsubvar);

    return 0;
}

static void finalize_map_removal(amxd_object_t* obj, bool update_datamodel) {
    inmap_t* map = NULL;
    bool dummy = false;
    const char* name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);

    if((name != NULL) && (strcmp(name, DUMMY_NAME) == 0)) {
        dummy = true;
    }

    if(update_datamodel) {
        amxd_trans_t trans;
        int rv = amxd_status_unknown_error;
        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, obj);
        if(dummy) {
            amxd_trans_select_pathf(&trans, ".^");
            amxd_trans_del_inst(&trans, amxd_object_get_index(obj), NULL);
        } else {
            amxd_trans_set_value(cstring_t, &trans, "AssignedExternalIPAddress", "");
            amxd_trans_set_value(uint32_t, &trans, "AssignedExternalPort", 0);
            amxd_trans_set_value(uint32_t, &trans, "AssignedExternalPortEndRange", 0);
            amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
        }

        rv = amxd_trans_apply(&trans, get_dm());
        if(rv != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Transaction failed: %d", rv);
        }
        amxd_trans_clean(&trans);
    }


    when_true_trace(dummy, exit, INFO, "Do not close firewall for dummy mapping");
    when_null(obj, exit);
    when_null(obj->priv, exit);
    map = (inmap_t*) obj->priv;

    if(map->firewall_opened) {
        close_firewall(map);
        map->firewall_opened = false;
    }

exit:
    return;
}

int disable_map(UNUSED amxd_object_t* templ, amxd_object_t* obj, UNUSED void* priv) {
    // when server is removed from backend it is not needed to remove individual maps from backend
    finalize_map_removal(obj, true);
    return 0;
}

static void add_or_update_inboundmapping(amxd_object_t* obj) {
    amxd_object_t* server_obj = get_server_by_mapping(obj);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_false_trace(amxd_object_get_value(bool, obj, "Enable", NULL), exit, INFO, "InboundMapping object is disabled");
    when_false_trace(server_is_ready(amxd_object_findf(obj, ".^.^")), exit, INFO, "Server is not ready.");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    var = amxc_var_add_key(amxc_htable_t, &args, "server", NULL);
    amxc_var_add_key(cstring_t, var, "id", name(server_obj));
    var = amxc_var_add_key(amxc_llist_t, &args, "map", NULL);
    append_map(NULL, obj, var);

    rv = amxm_execute_function("pcp", "pcp", "map-add", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to add inbound mapping");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return;
}

static void remove_inboundmapping(amxd_object_t* obj, bool update_datamodel) {
    amxd_object_t* server_obj = get_server_by_mapping(obj);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    amxc_var_t* subvar = NULL;
    const char* mid = name(obj);
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_false(server_is_ready(amxd_object_findf(obj, ".^.^")), exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    var = amxc_var_add_key(amxc_htable_t, &args, "server", NULL);
    amxc_var_add_key(cstring_t, var, "id", name(server_obj));
    var = amxc_var_add_key(amxc_llist_t, &args, "map", NULL);
    subvar = amxc_var_add(amxc_htable_t, var, NULL);
    amxc_var_add_key(cstring_t, subvar, "id", mid);

    rv = amxm_execute_function("pcp", "pcp", "map-remove", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to remove inbound mapping");

exit:
    finalize_map_removal(obj, update_datamodel);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void lifetime_expired(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_object_t* obj = (amxd_object_t*) priv;
    amxd_trans_t trans;
    int rv = amxd_status_unknown_error;

    when_null(obj, exit);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_del_inst(&trans, amxd_object_get_index(obj), NULL);
    rv = amxd_trans_apply(&trans, get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed: %d", rv);
    }
    amxd_trans_clean(&trans);

exit:
    return;
}

static void map_set_lifetime(inmap_t* map) {
    uint32_t lifetime = amxd_object_get_value(uint32_t, map->obj, "Lifetime", NULL);

    if(lifetime > 0) { // similar to Firewall LeaseDuration, start timer as soon as instance is added
        amxp_timer_start(map->timer, lifetime * 1000);
        amxd_object_set_attr(map->obj, amxd_oattr_persistent, false);
    } else {
        amxp_timer_stop(map->timer);
        amxd_object_set_attr(map->obj, amxd_oattr_persistent, true);
    }

    if(map->firewall_opened) {
        open_or_update_firewall(map);
    }
}

inmap_t* new_inboundmapping(amxd_object_t* obj) {
    inmap_t* map = NULL;

    map = calloc(1, sizeof(inmap_t));
    when_null(map, exit);

    map->obj = obj;

    amxp_timer_new(&map->timer, lifetime_expired, obj);

    map_set_lifetime(map);

    free(obj->priv);
    obj->priv = map;

exit:
    return map;
}

void _inboundmapping_added(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));

    new_inboundmapping(obj);
    add_or_update_inboundmapping(obj);
}

void _inboundmapping_changed(UNUSED const char* const sig_name,
                             const amxc_var_t* const data,
                             UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    amxc_var_t* enable_var = GETP_ARG(data, "parameters.Enable.to");
    bool is_disabling = (enable_var != NULL) && !GET_BOOL(enable_var, NULL);

    if(is_disabling) {
        remove_inboundmapping(obj, true);
    } else {
        add_or_update_inboundmapping(obj);
    }
}

void _lifetime_changed(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    when_null(obj, exit);
    when_null(obj->priv, exit);
    map_set_lifetime((inmap_t*) obj->priv);
exit:
    return;
}

void _filter_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* map_obj = amxd_object_findf(templ, ".^");

    add_or_update_inboundmapping(map_obj);
}

void _filter_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxd_object_t* filter_obj = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* map_obj = get_mapping_by_filter(filter_obj);

    add_or_update_inboundmapping(map_obj);
}

amxd_status_t _inboundmapping_destroy(amxd_object_t* obj,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_true_status(amxd_object_get_type(obj) == amxd_object_template,
                     exit,
                     status = amxd_status_ok);

    if(amxd_object_get_value(bool, obj, "Enable", NULL)) {
        remove_inboundmapping(obj, false);
    }

    if(obj->priv != NULL) {
        inmap_t* map = (inmap_t*) obj->priv;
        amxp_timer_delete(&map->timer);
        FREE(obj->priv);
    }

    status = amxd_status_ok;
exit:
    return status;
}

static pcpstatus_t to_pcpstatus(const char* txt) {
    pcpstatus_t pcpstatus = ERROR;

    when_str_empty(txt, exit);

    if(strcmp(txt, "Enabled") == 0) {
        pcpstatus = SUCCESS;
    }

exit:
    return pcpstatus;
}

int inboundmapping_state_changed(UNUSED const char* function_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    const char* cid = GET_CHAR(args, "client-id");
    const char* sid = GET_CHAR(args, "server-id");
    const char* mid = GET_CHAR(args, "map-id");
    const char* external_ip = GET_CHAR(args, "external-ip");
    uint32_t external_port = GET_UINT32(args, "external-port");
    amxd_object_t* obj = NULL;
    pcpstatus_t status = to_pcpstatus(GET_CHAR(args, "status"));
    amxd_trans_t trans;
    amxd_status_t rv = amxd_status_unknown_error;
    inmap_t* map = NULL;
    const char* map_alias = NULL;
    bool dummy = false;
    char* external_ipv4 = NULL;

    amxd_trans_init(&trans);

    when_str_empty_trace(cid, exit, ERROR, "Missing client id");
    when_str_empty_trace(sid, exit, ERROR, "Missing server id");
    when_str_empty_trace(mid, exit, ERROR, "Missing map id");

    obj = amxd_dm_findf(get_dm(), "PCP.Client.[Alias=='%s'].Server.[Alias=='%s'].InboundMapping.[Alias=='%s']", cid, sid, mid);

    when_null_trace(obj, exit, ERROR, "Object[PCP.Client.%s.Server.%s.InboundMapping.%s.] not found", cid, sid, mid);
    when_null_trace(obj->priv, exit, ERROR, "Object[PCP.Client.%s.Server.%s.InboundMapping.%s.] has no private data", cid, sid, mid);
    map = (inmap_t*) obj->priv;

    map_alias = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
    if((map_alias != NULL) && (strcmp(map_alias, DUMMY_NAME) == 0)) {
        dummy = true;
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", pcpstatus_to_str(status));
    if(status == SUCCESS) {
        // Convert IPv4-mapped-to-IPv6 to IPv4
        external_ipv4 = ipat_text_convert(external_ip, AF_INET, false);
        if(external_ipv4) {
            external_ip = external_ipv4;
        }
        amxd_trans_set_value(cstring_t, &trans, "AssignedExternalIPAddress", external_ip);
        amxd_trans_set_value(uint32_t, &trans, "AssignedExternalPort", external_port);
        if(dummy) {
            amxd_trans_select_pathf(&trans, ".^.^");
            amxd_trans_set_value(cstring_t, &trans, "ExternalIPAddress", external_ip);
        }
    }
    rv = amxd_trans_apply(&trans, get_dm());
    when_false_trace(rv == amxd_status_ok, exit, ERROR, "Transaction failed");

    when_true_trace(dummy, exit, INFO, "The mapping is a dummy, don't open firewall");

    if(status == SUCCESS) {
        bool retval = false;

        retval = open_or_update_firewall(map);
        when_false_trace(retval, exit, ERROR, "InboundMapping[%s] failed to %s firewall", mid, map->firewall_opened ? "open" : "update");
        map->firewall_opened = true;
    } else {
        if(map->firewall_opened) {
            close_firewall(map);
            map->firewall_opened = false;
        }
    }

exit:
    amxd_trans_clean(&trans);
    free(external_ipv4);
    return 0;
}
