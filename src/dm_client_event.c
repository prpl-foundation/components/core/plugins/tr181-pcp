/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "pcp.h"
#include "dm_pcp.h"
#include <amxd/amxd_transaction.h>
#include <netmodel/common_api.h>

#define ME "pcp"
#define DHCPV6_OPTION_AFTR 64
#define wan_interface(CLIENT) object_da_string(CLIENT->object, "WANInterface");

bool client_is_ready(amxd_object_t* obj) {
    client_t* pcp_cfg = (client_t*) obj->priv;
    return pcp_cfg == NULL ? false : pcp_cfg->ready;
}

const char* client_get_nameoraddress(amxd_object_t* obj) {
    client_t* pcp_cfg = (client_t*) obj->priv;
    return pcp_cfg == NULL ? NULL : pcp_cfg->nameoraddress;
}

const char* client_get_srcaddress(amxd_object_t* obj) {
    client_t* pcp_cfg = (client_t*) obj->priv;
    return pcp_cfg == NULL ? NULL : pcp_cfg->srcaddress;
}

static void dhcpv6_query_cb(UNUSED const char* sig_name, const amxc_var_t* result, void* priv) {
    client_t* pcp_cfg = (client_t*) priv;
    const char* aftr_name = GET_CHAR(result, NULL);

    when_null_trace(pcp_cfg, exit, ERROR, "Userdata is empty");

    SAH_TRACEZ_INFO(ME, "DHCPv6 option AFTR_NAME changed from[%s] to[%s]", pcp_cfg->nameoraddress, aftr_name);

    FREE(pcp_cfg->nameoraddress);

    if(string_empty(aftr_name)) {
        all_servers_set_nameoraddress(pcp_cfg->object, "");
    } else {
        pcp_cfg->nameoraddress = strdup(aftr_name);
        all_servers_set_nameoraddress(pcp_cfg->object, aftr_name);
    }

exit:
    return;
}

static void enable_1_client(client_t* pcp_cfg) {
    SAH_TRACEZ_INFO(ME, "Enable server entries");

    if(pcp_cfg->dhcpv6_query == NULL) {
        const char* interface = wan_interface(pcp_cfg);
        when_str_empty_trace(interface, skip_dhcpv6, WARNING, "Interface is empty");

        pcp_cfg->dhcpv6_query = netmodel_openQuery_getDHCPOption(interface, "PCP", "req6", DHCPV6_OPTION_AFTR, netmodel_traverse_down, dhcpv6_query_cb, pcp_cfg);
        if(pcp_cfg->dhcpv6_query == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to open dhcpv6 option query");
        }
    }

skip_dhcpv6:
    amxd_object_for_all(pcp_cfg->object, "Server.[Enable==true && Origin=='Static'].", enable_server, NULL);
    pcp_cfg->ready = true;
    set_status(pcp_cfg->object, SUCCESS);
}

static void disable_1_client(client_t* pcp_cfg) {
    pcp_cfg->ready = false;

    amxd_object_for_all(pcp_cfg->object, "Server.[Enable==true].", disable_server, NULL);
    CLOSE_QUERY(pcp_cfg->dhcpv6_query);
    FREE(pcp_cfg->nameoraddress);
}

static void addr_query_cb(UNUSED const char* sig_name,
                          const amxc_var_t* data,
                          void* priv) {
    client_t* pcp_cfg = (client_t*) priv;
    const char* addr = GET_CHAR(data, NULL);

    SAH_TRACEZ_INFO(ME, "IPv6 GUA changed to %s", addr);

    FREE(pcp_cfg->srcaddress);

    if(!string_empty(addr)) {
        pcp_cfg->srcaddress = strdup(addr);
        enable_1_client(pcp_cfg);
    } else {
        disable_1_client(pcp_cfg);
        set_status(pcp_cfg->object, ERROR);
    }
}

static bool enable_2_client(client_t* pcp_cfg) {
    const char* interface = wan_interface(pcp_cfg);
    bool rv = false;

    when_str_empty_trace(interface, exit, WARNING, "Interface is empty");

    SAH_TRACEZ_INFO(ME, "Create Netmodel luckyAddrAddress query for [%s]", interface);

    pcp_cfg->addr_query = netmodel_openQuery_luckyAddrAddress(interface, "PCP", "ipv6 && global", netmodel_traverse_down, addr_query_cb, (void*) pcp_cfg);
    when_null_trace(pcp_cfg->addr_query, exit, ERROR, "Failed to open getAddr query");

    rv = true;

exit:
    if(!rv) {
        set_status(pcp_cfg->object, ERROR);
    }
    return rv;
}

static void disable_2_client(client_t* pcp_cfg, bool update_status) {

    disable_1_client(pcp_cfg);

    CLOSE_QUERY(pcp_cfg->addr_query);
    FREE(pcp_cfg->netdevname);
    FREE(pcp_cfg->srcaddress);

    if(update_status) {
        set_status(pcp_cfg->object, ERROR);
    }
}

static void disable_client(client_t* pcp_cfg, bool update_status) {
    when_null(pcp_cfg, exit);
    disable_2_client(pcp_cfg, false);
    CLOSE_QUERY(pcp_cfg->netdevname_query);
exit:
    if(update_status) {
        set_status(pcp_cfg->object, DISABLED);
    }
}

static void netdevname_query_cb(UNUSED const char* sig_name,
                                const amxc_var_t* data,
                                void* priv) {
    client_t* pcp_cfg = (client_t*) priv;
    const char* netdevname = GET_CHAR(data, NULL);

    SAH_TRACEZ_INFO(ME, "NetDevName changed from '%s' to '%s'", pcp_cfg->netdevname, netdevname);

    if(!string_empty(pcp_cfg->netdevname)) {
        disable_2_client(pcp_cfg, string_empty(netdevname));
    }

    when_str_empty(netdevname, exit);

    pcp_cfg->netdevname = strdup(netdevname);
    enable_2_client(pcp_cfg);

exit:
    return;
}

static bool enable_client(client_t* pcp_cfg) {
    pcpstatus_t status = ERROR;
    const char* interface = NULL;
    bool enable = false;
    bool rv = false;

    when_false_status(stack_is_enabled(), exit, status = STACKDISABLED);
    enable = amxd_object_get_value(bool, pcp_cfg->object, "Enable", NULL);
    when_false_status(enable, exit, status = LAST);

    interface = wan_interface(pcp_cfg);
    when_str_empty_trace(interface, exit, ERROR, "Interface is empty");

    pcp_cfg->netdevname_query = netmodel_openQuery_getFirstParameter(interface, "PCP", "NetDevName", "netdev-bound", netmodel_traverse_down, netdevname_query_cb, (void*) pcp_cfg);
    when_null_trace(pcp_cfg->netdevname_query, exit, ERROR, "Failed to open NetDevName query");
    status = LAST;
    rv = false;
exit:
    if(status != LAST) {
        set_status(pcp_cfg->object, status);
    }
    return rv;
}

client_t* new_client(amxd_object_t* obj) {
    client_t* pcp_cfg = NULL;
    pcpstatus_t status = ERROR;

    pcp_cfg = calloc(1, sizeof(client_t));
    when_null(pcp_cfg, exit);
    pcp_cfg->object = obj;
    obj->priv = pcp_cfg;

    enable_client(pcp_cfg);

    status = LAST;

exit:
    if(status != LAST) {
        set_status(obj, status);
    }
    return pcp_cfg;
}

void _client_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));

    new_client(obj);
}

void _client_enable_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    client_t* pcp_cfg = (client_t*) obj->priv;
    bool enable = false;

    when_null(pcp_cfg, exit);
    when_false(stack_is_enabled(), exit);

    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
    if(!enable) {
        disable_client(pcp_cfg, true);
    } else {
        enable_client(pcp_cfg);
    }

exit:
    return;
}

amxd_status_t _read_upnpiwf_status(UNUSED amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   UNUSED const amxc_var_t* const args,
                                   amxc_var_t* const retval,
                                   UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    bool enable = false;

    if(reason != action_param_read) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    enable = amxd_object_get_value(bool, object, "Enable", NULL);

    if(enable) {
        amxc_var_set_cstring_t(retval, "Enabled");
    } else {
        amxc_var_set_cstring_t(retval, "Disabled");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static int stack_disabled(UNUSED amxd_object_t* templ, amxd_object_t* obj, UNUSED void* priv) {
    client_t* pcp_cfg = (client_t*) obj->priv;

    when_null(pcp_cfg, exit);

    disable_client(pcp_cfg, false);
    set_status(obj, STACKDISABLED);
exit:
    return 0;
}

static int stack_enabled(UNUSED amxd_object_t* templ, amxd_object_t* obj, UNUSED void* priv) {
    client_t* pcp_cfg = (client_t*) obj->priv;

    when_null(pcp_cfg, exit);

    enable_client(pcp_cfg);
exit:
    return 0;
}

void all_clients_enable_changed(amxd_object_t* obj, bool enable) {
    amxd_object_for_all(obj, "Client.[Enable==true].", enable ? stack_enabled : stack_disabled, NULL);
}

static void delete_client(amxd_object_t* obj) {
    client_t* pcp_cfg = (client_t*) obj->priv;

    when_null(pcp_cfg, exit);

    disable_client(pcp_cfg, false);
    FREE(obj->priv);
exit:
    return;
}

amxd_status_t _client_destroy(amxd_object_t* obj,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              UNUSED amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_true_status(amxd_object_get_type(obj) == amxd_object_template,
                     exit,
                     status = amxd_status_ok);

    delete_client(obj);

    status = amxd_status_ok;
exit:
    return status;
}