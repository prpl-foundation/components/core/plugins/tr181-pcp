/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace_macros.h>

#include "pcp.h"

#include <amxm/amxm.h>
#include <amxd/amxd_path.h>

#define ME "pcp"

bool open_or_update_firewall(inmap_t* map) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;
    amxd_object_t* server_obj = NULL;
    amxd_object_t* client_obj = NULL;
    const char* internal_ip = NULL;
    const char* interface = NULL;
    uint32_t lease_duration = 0;
    int port = 0;
    int protocol = 0;
    const char* id = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null(map, exit);
    server_obj = get_server_by_mapping(map->obj);
    client_obj = get_client_by_server(server_obj);
    internal_ip = object_da_string(map->obj, "ThirdPartyAddress");
    interface = object_da_string(client_obj, "WANInterface");
    lease_duration = amxd_object_get_value(uint32_t, map->obj, "Lifetime", NULL);
    id = amxd_object_get_name(map->obj, AMXD_OBJECT_NAMED);

    get_port_and_protocol(map->obj, &port, &protocol, -1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);
    amxc_var_add_key(cstring_t, &args, "origin", "System");
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    amxc_var_add_key(uint32_t, &args, "destination_port", port);
    amxc_var_add_key(cstring_t, &args, "internal_client", internal_ip);
    amxc_var_add_key(int32_t, &args, "protocol", protocol);
    amxc_var_add_key(uint32_t, &args, "lease_duration", lease_duration);
    amxc_var_add_key(bool, &args, "enable", true);

    SAH_TRACEZ_INFO(ME, "Execute fw's set_pinhole");
    rv = amxm_execute_function("fw", "fw", "set_pinhole", &args, &ret);
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Firewall failed to add pinhole[%s]", id);
        goto exit;
    }

    SAH_TRACEZ_NOTICE(ME, "Firewall pinhole[%s] added", id);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == 0;
}

bool close_firewall(inmap_t* map) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;
    const char* id = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null(map, exit);
    id = amxd_object_get_name(map->obj, AMXD_OBJECT_NAMED);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);

    SAH_TRACEZ_INFO(ME, "Execute fw's delete_pinhole");
    rv = amxm_execute_function("fw", "fw", "delete_pinhole", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Firewall pinhole[%s] could not be removed", id);

    SAH_TRACEZ_NOTICE(ME, "Firewall pinhole[%s] removed", id);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == 0;
}
