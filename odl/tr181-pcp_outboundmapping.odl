%define {
    select PCP.Client.Server {
        /**
         * Not supported.
         *
         * Outbound Mapping table.
         *
         * This table contains all Outbound Mappings requested by this PCP Client on this PCP Server.
         * Such a mapping is created by a PCP request with OpCode PEER, as described in [Section 12/RFC6887].
         *
         * These requests can be issued from internal device applications, from static configuration or from other LAN device applications via interworking functions such as UPnP IGD or PCP proxies.
         * The Origin parameter indicates which mechanism requested the mapping.
         *
         * For non-Static mappings (here Static refers to the mechanism that created the mapping, not to the Lifetime), the Controller MAY modify the Enable parameter but MUST NOT modify any other parameters in the mapping or its sub-objects.
         * @version 1.0
         */
        %persistent object OutboundMapping[] {
            counted with OutboundMappingNumberOfEntries;

            /**
             * A non-volatile unique key used to reference this instance.
             * @version 1.0
             */
            %persistent %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * Not supported.
             * Enables or disables this OutboundMapping instance.
             * @version 1.0
             */
            %persistent bool Enable = false;

            /**
             * Not supported.
             * The status of this entry. 
             * Enumeration of: Disabled, Enabled, Error_PCPErrorCodeReceived, Error.
             * @version 1.0
             */
            %read-only string Status = "Disabled" {
                on action validate call check_enum ["Disabled", "Enabled", "Error_PCPErrorCodeReceived", "Error"];
            }

            /**
             * Not supported.
             * Provides the PCP error code when Status is Error_PCPErrorCodeReceived.
             * Error code values are defined in [Section 7.4/RFC6887].
             * @version 1.0
             */
            %read-only uint32 ErrorCode = 0;

            /**
             * Not supported.
             * Mechanism via which the mapping was requested. 
             * Enumeration of:
             *   Internal (Requested via internal device application)
             *   UPnP_IWF (Requested via UPnP IGD interworking function)
             *   PCP_Proxy (Requested via PCP proxy)
             *   Static (Requested via static configuration, i.e. created by the Controller, by some other management entity (e.g. via a GUI), or is present in the factory default configuration)
             * @version 1.0
             */
            %persistent %read-only string Origin = "Internal" {
                on action validate call check_enum ["Internal", "UPnP_IWF", "PCP_Proxy", "Static"];
            }

            /**
             * Not supported.
             * Determines the time to live, in seconds, of this Outbound Mapping lease, i.e. the remaining time before this port mapping expires.
             * A value of 0 means that the port mapping is permanent (referred to as a static mapping in [RFC6887]).
             * @version 1.0
             */
            %persistent uint32 Lifetime = 0;

            /**
             * Not supported.
             * The external IPv4 or IPv6 Address that the PCP-controlled device will use to send outgoing packets covered by this mapping.
             * This is useful for refreshing a mapping, especially after the PCP Server has lost state.
             * If the PCP Client does not know the external address, or does not have a preference, it MUST use an empty string.
             * @version 1.0
             */
            %persistent string SuggestedExternalIPAddress = "" {
                on action validate call check_maximum_length 45;
            }

            /**
             * Not supported.
             * The external port that the PCP-controlled device will use to send outgoing packets covered by this mapping.
             * This is useful for refreshing a mapping, especially after the PCP Server has lost state.
             * If the PCP Client does not know the external port, or does not have a preference, it MUST use 0.
             * @version 1.0
             */
            %persistent uint32 SuggestedExternalPort = 0;

            /**
             * Not supported.
             * The remote peer's IP address, as seen from the PCP Client, for this Outbound Mapping.
             * If the value isn't assigned by the Controller on creation, the Agent MUST choose an initial value that (together with InternalPort, ProtocolNumber, ThirdPartyAddress, RemotePort) doesn't conflict with any existing entries.
             * @version 1.0
             */
            %persistent %key string RemoteHostIPAddress {
                on action validate call check_maximum_length 45;
            }

            /**
             * Not supported.
             * The port on PCP Client WAN Interface that the PCP-controlled device SHOULD listen to for this mapping.
             * If the value isn't assigned by the Controller on creation, the Agent MUST choose an initial value that (together with ProtocolNumber, ThirdPartyAddress, RemoteHostIPAddress, RemotePort) doesn't conflict with any existing entries.
             * @version 1.0
             */
            %persistent %key uint32 InternalPort;

            /**
             * Not supported.
             * The remote peer's port, as seen from the PCP Client, for this Outbound Mapping.
             * If the value isn't assigned by the Controller on creation, the Agent MUST choose an initial value that (together with InternalPort, ProtocolNumber, ThirdPartyAddress, RemoteHostIPAddress) doesn't conflict with any existing entries.
             * @version 1.0
             */
            %persistent %key uint32 RemotePort;

            /**
             * Not supported.
             * The protocol number of the OutboundMapping. Values are taken from [IANA-protocolnumbers].
             * The value -1 means all protocols.
             * If the value isn't assigned by the Controller on creation, the Agent MUST choose an initial value that (together with InternalPort, ThirdPartyAddress, RemoteHostIPAddress, RemotePort) doesn't conflict with any existing entries.
             * @version 1.0
             */
            %persistent %key int32 ProtocolNumber;

            /**
             * Not supported.
             * Under certain conditions, the PCP Client can create a PCP mapping on behalf of another device, by using the THIRD_PARTY option, as specified in [Section 7.3/RFC6887].
             * In that case, ThirdPartyAddress is the IP address of the device for which the PCP operation is requested.
             * For non-third-party mappings, ThirdPartyAddress SHOULD be an empty string.
             * If the value isn't assigned by the Controller on creation, the Agent MUST choose an initial value that (together with InternalPort, ProtocolNumber, RemoteHostIPAddress, RemotePort) doesn't conflict with any existing entries.
             * @version 1.0
             */
            %persistent %key string ThirdPartyAddress {
                on action validate call check_maximum_length 45;
            }

            /**
             * Not supported.
             * User-readable description of this OutboundMapping.
             * @version 1.0
             */
            %persistent string Description = "" {
                on action validate call check_maximum_length 256;
            }

            /**
             * Not supported.
             * The external IP address returned by the PCP Server.
             * The IP address that the PCP-controlled device uses to send outgoing packets corresponding to this mapping.
             * @version 1.0
             */
            %read-only string AssignedExternalIPAddress = "";

            /**
             * Not supported.
             * The external port returned by the PCP Server.
             * The port that the PCP-controlled device uses to send outgoing packets corresponding to this mapping.
             * @version 1.0
             */
            %read-only uint32 AssignedExternalPort = 0;
        }
    }
}

%populate { 
}
