/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcp.h"
#include <pcp-client/pcp.h>

#define ME "pcp"
#define string_empty(X) ((X == NULL) || (*X == '\0'))
#define PCP_MAX_SUPPORTED_VERSION 2
#define CLOSE_FLOWS 1
#define LOG_FMT "libpcp: %s"

typedef struct {
    pcp_ctx_t* pcp_ctx;
    amxp_timer_t* timer;
    amxc_htable_t maps;          // list of map_t
    char* client_id;
    amxc_htable_it_t it;
} server_t;

typedef struct {
    pcp_flow_t* flow;
    amxc_htable_it_t it;
} map_t;

static amxm_module_t* mod = NULL;
static amxc_htable_t servers;    // list of server_t

static void map_it_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    map_t* m = (map_t*) amxc_container_of(it, map_t, it);
    free(m);
}

static inline unsigned int to_milliseconds(struct timeval* tv) {
    return (tv->tv_sec * (unsigned int) 1000) + (tv->tv_usec / 1000);
}

static void handle_pcp_events_and_start_timer(server_t* s) {
    struct timeval timeout = {0, 0};

    //process all events and get timeout value for next select
    pcp_pulse(s->pcp_ctx, &timeout);

    if((timeout.tv_usec == 0) && (timeout.tv_sec == 0)) {
        SAH_TRACEZ_WARNING(ME, "Libpcp suggests timeout 0 s 0 us");
        amxp_timer_stop(s->timer);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Start timer[%ld s %ld us]", timeout.tv_sec, (long int) timeout.tv_usec);
    amxp_timer_start(s->timer, to_milliseconds(&timeout));

exit:
    return;
}

static server_t* find_server_by_id(const char* id) {
    server_t* s = NULL;
    amxc_htable_it_t* it = amxc_htable_get(&servers, id);

    when_null(it, exit);

    s = amxc_container_of(it, server_t, it);
exit:
    return s;
}

static map_t* find_map_by_id(server_t* s, const char* id) {
    map_t* m = NULL;
    amxc_htable_it_t* it = amxc_htable_get(&s->maps, id);

    when_null(it, exit);

    m = amxc_container_of(it, map_t, it);

exit:
    return m;
}

// inspired by libpcp's example
static int sock_pton(const char* hostname, int port, struct sockaddr* sa) {
    struct addrinfo hints = {0};
    struct addrinfo* result = NULL;
    struct addrinfo* rp4 = NULL;
    struct addrinfo* rp6 = NULL;
    int rv = -1;
    amxc_var_t var;

    amxc_var_init(&var);
    if(port > 0) {
        amxc_var_set(int32_t, &var, port);
        when_failed_trace(amxc_var_cast(&var, AMXC_VAR_ID_CSTRING), exit, ERROR, "Failed to convert port %d", port);
    }

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_V4MAPPED;

    rv = getaddrinfo(hostname, port > 0 ? GET_CHAR(&var, NULL) : NULL, &hints, &result);
    when_false_trace(rv == 0, exit, ERROR, "Failed to resolve hostname[%s]", hostname);

    for(struct addrinfo* rp = result; rp != NULL; rp = rp->ai_next) {
        if((rp6 == NULL) && (rp->ai_family == AF_INET6)) {
            rp6 = rp;
            break;
        }
        if((rp4 == NULL) && (rp->ai_family == AF_INET)) {
            rp4 = rp;
        }
    }

    if(rp6 != NULL) {
        memcpy(sa, rp6->ai_addr, sizeof(struct sockaddr_in6));
    } else if(rp4 != NULL) {
        memcpy(sa, rp4->ai_addr, sizeof(struct sockaddr_in));
    } else {
        SAH_TRACEZ_ERROR(ME, "No valid ip address for hostname[%s]", hostname);
    }

    freeaddrinfo(result);

exit:
    amxc_var_clean(&var);
    return rv;
}

static bool add_map(server_t* s, map_t* m, amxc_var_t* args) {
    const char* thirdparty_addr = GET_CHAR(args, "thirdparty_address");
    const char* src_addr = GET_CHAR(args, "srcaddress");
    uint32_t lifetime = GET_UINT32(args, "lifetime");
    struct sockaddr_storage sa = {0};
    bool rv = false;

    when_str_empty_trace(src_addr, exit, ERROR, "Cannot add mapping because of empty src address");

    if(lifetime == 0) {
        // Static/permanent mapping, request maximum lifetime
        lifetime = UINT32_MAX;
    }

    sock_pton(src_addr, GET_INT32(args, "port"), (struct sockaddr*) &sa);
    m->flow = pcp_new_flow(s->pcp_ctx,
                           (struct sockaddr*) &sa,
                           NULL,
                           NULL,
                           GET_INT32(args, "protocol"),
                           lifetime,
                           m);
    when_null_trace(m->flow, exit, ERROR, "Failed to add new flow");

    amxc_var_for_each(filter, GET_ARG(args, "filter")) {
        sock_pton(GET_CHAR(filter, "ipaddress"), GET_INT32(filter, "port"), (struct sockaddr*) &sa);
        pcp_flow_set_filter_opt(m->flow, (struct sockaddr*) &sa, GET_INT32(filter, "prefix"));
    }

    if(!string_empty(thirdparty_addr)) {
        int pton_rv = -1;
        struct sockaddr_in* psa = (struct sockaddr_in*) &sa;
        struct sockaddr_in6* psa6 = (struct sockaddr_in6*) &sa;

        memset(&sa, 0, sizeof(struct sockaddr_storage));

        pton_rv = inet_pton(AF_INET6, thirdparty_addr, &psa6->sin6_addr);
        if(rv == 1) {
            sa.ss_family = AF_INET6;
        } else {
            pton_rv = inet_pton(AF_INET, thirdparty_addr, &psa->sin_addr);
            sa.ss_family = AF_INET;
        }
        if(pton_rv != 1) {
            SAH_TRACEZ_ERROR(ME, "Option third party: bad address[%s]", thirdparty_addr);
            pcp_close_flow(m->flow);
            pcp_delete_flow(m->flow);
            m->flow = NULL;
            goto exit;
        } else {
            pcp_flow_set_3rd_party_opt(m->flow, (struct sockaddr*) &sa);
        }
    }

    pcp_flow_set_user_data(m->flow, m);
    rv = true;

exit:
    return rv;
}

static void timeout_cb(UNUSED amxp_timer_t* const timer, void* data) {
    server_t* s = (server_t*) data;

    SAH_TRACEZ_INFO(ME, "Server[%s]'s timer expired", amxc_htable_it_get_key(&s->it));

    handle_pcp_events_and_start_timer(s);
}

static void modpcp_read_handler(UNUSED int fd, void* priv) {
    server_t* s = (server_t*) priv;

    when_null_trace(s, exit, ERROR, "No server");

    handle_pcp_events_and_start_timer(s);

exit:
    return;
}

static const char* state_of(pcp_fstate_e e) {
    const char* state = NULL;

    switch(e) {
    case pcp_state_succeeded:
        state = "Enabled";
        break;
    default:
        state = "Error";
    }

    return state;
}

static void flow_change_notify(pcp_flow_t* f, UNUSED struct sockaddr* src_addr, UNUSED struct sockaddr* ext_addr, pcp_fstate_e e, void* cb_arg) {
    map_t* m = (map_t*) pcp_flow_get_user_data(f);
    server_t* s = (server_t*) cb_arg;
    pcp_flow_info_t* info = NULL;
    size_t cnt = 0;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    info = pcp_flow_get_info(f, &cnt);
    when_null_trace(info, exit, ERROR, "Missing flow info");

    SAH_TRACEZ_INFO(ME, "Map[%s] state changed to [%s]", amxc_htable_it_get_key(&m->it), state_of(e));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "server-id", amxc_htable_it_get_key(&s->it));
    amxc_var_add_key(cstring_t, &args, "client-id", s->client_id);
    amxc_var_add_key(cstring_t, &args, "map-id", amxc_htable_it_get_key(&m->it));
    amxc_var_add_key(cstring_t, &args, "status", state_of(e));
    if(e == pcp_state_succeeded) {
        char ntop_buff[INET6_ADDRSTRLEN];
        inet_ntop(AF_INET6, &info->ext_ip, ntop_buff, sizeof(ntop_buff));
        amxc_var_add_key(cstring_t, &args, "external-ip", ntop_buff);
        amxc_var_add_key(uint32_t, &args, "external-port", ntohs(info->ext_port));
    }

    amxm_execute_function(NULL, "core", "map-state-changed", &args, &ret);

exit:
    free(info);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static int modpcp_map_add(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxc_var_t* map = GETP_ARG(args, "map.0");
    const char* sid = GETP_CHAR(args, "server.id");
    const char* mid = GET_CHAR(map, "id");
    server_t* s = NULL;
    map_t* m = NULL;

    when_str_empty_trace(sid, exit, ERROR, "Missing server.id");
    when_str_empty_trace(mid, exit, ERROR, "Missing map.0.id");

    s = find_server_by_id(sid);

    when_null_trace(s, exit, ERROR, "No server[%s] to add map[%s] to", sid, mid);

    m = find_map_by_id(s, mid);

    if(m == NULL) {
        m = calloc(1, sizeof(map_t));
        amxc_htable_insert(&s->maps, mid, &m->it);
    } else if(m->flow != NULL) {
        // send PCP message with lifetime set to 0
        pcp_close_flow(m->flow);
        pcp_delete_flow(m->flow);
        m->flow = NULL;
    }

    if(!add_map(s, m, map)) {
        SAH_TRACEZ_WARNING(ME, "Failed to send MAP request for map[%s]", mid);
        amxc_htable_it_clean(&m->it, map_it_delete);
        m = NULL;
        goto exit;
    }

    // to send MAP request
    handle_pcp_events_and_start_timer(s);

exit:
    return 0;
}

static int modpcp_map_remove(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    const char* sid = GETP_CHAR(args, "server.id");
    const char* mid = GETP_CHAR(args, "map.0.id");
    server_t* s = NULL;
    map_t* m = NULL;

    when_str_empty_trace(sid, exit, ERROR, "Missing server.id");
    when_str_empty_trace(mid, exit, ERROR, "Missing map.0.id");

    s = find_server_by_id(sid);

    when_null_trace(s, exit, ERROR, "No server[%s] to remove map[%s] from", sid, mid);

    m = find_map_by_id(s, mid);

    when_null_trace(m, exit, ERROR, "Server[%s] has no map[%s] to remove", sid, mid);

    // send PCP message with lifetime set to 0
    pcp_close_flow(m->flow);
    pcp_delete_flow(m->flow);
    m->flow = NULL;
    amxc_htable_it_clean(&m->it, map_it_delete);
    m = NULL;

exit:
    return 0;
}

static int modpcp_server_add(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    server_t* s = NULL;
    const char* sid = GETP_CHAR(args, "server.id");
    const char* cid = GETP_CHAR(args, "server.client-id");
    const char* hostname = GETP_CHAR(args, "server.ipaddress");
    int port = GETP_INT32(args, "server.port");
    struct sockaddr_storage sa;

    SAH_TRACEZ_INFO(ME, "Using server[%s:%d]", hostname, port);

    s = calloc(1, sizeof(server_t));
    when_null(s, exit);

    amxc_htable_insert(&servers, sid, &s->it);
    s->pcp_ctx = pcp_init(DISABLE_AUTODISCOVERY, NULL);
    amxp_timer_new(&s->timer, timeout_cb, s);
    amxc_htable_init(&s->maps, 0);
    s->client_id = strdup(cid);

    sock_pton(hostname, port, (struct sockaddr*) &sa);
    pcp_add_server(s->pcp_ctx, (struct sockaddr*) &sa, GETP_UINT32(args, "server.version"));

    amxc_var_for_each(map, GET_ARG(args, "map")) {
        const char* id = GET_CHAR(map, "id");
        map_t* m = NULL;

        m = calloc(1, sizeof(map_t));

        if(!add_map(s, m, map)) {
            SAH_TRACEZ_WARNING(ME, "Failed to send MAP request for map[%s]", id);
            amxc_htable_it_clean(&m->it, map_it_delete);
            m = NULL;
        } else {
            amxc_htable_insert(&s->maps, id, &m->it);
        }
    }

    pcp_set_flow_change_cb(s->pcp_ctx, flow_change_notify, s);

    amxp_connection_add(pcp_get_socket(s->pcp_ctx), modpcp_read_handler, NULL, AMXP_CONNECTION_CUSTOM, s);

    if(amxc_htable_get_first(&s->maps) != NULL) { // otherwise pcp_pulse returns timeout 0 and we stuck in amxp_timer loop
        handle_pcp_events_and_start_timer(s);
    }

exit:
    return 0;
}

static void server_it_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    server_t* s = amxc_container_of(it, server_t, it);
    int fd = -1;

    fd = pcp_get_socket(s->pcp_ctx);

    // send PCP message with lifetime set to 0
    pcp_terminate(s->pcp_ctx, CLOSE_FLOWS);

    free(s->pcp_ctx);
    s->pcp_ctx = NULL;

    amxp_connection_remove(fd);
    amxp_timer_delete(&s->timer);
    amxc_htable_clean(&s->maps, map_it_delete);
    amxc_htable_it_clean(&s->it, NULL);
    free(s->client_id);
    s->client_id = NULL;
    free(s);
}

static int modpcp_server_remove(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    const char* id = GETP_CHAR(args, "server.id");
    server_t* s = NULL;

    when_str_empty_trace(id, exit, ERROR, "Missing server.id");

    s = find_server_by_id(id);

    when_null_trace(s, exit, ERROR, "No server[%s] to remove", id);

    server_it_delete(NULL, &s->it);
    s = NULL;

exit:
    return 0;
}

static int modpcp_supported_versions(UNUSED const char* function_name,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxc_var_set(cstring_t, ret, "0,1,2");

    return 0;
}

static int modpcp_supported_options(UNUSED const char* function_name,
                                    UNUSED amxc_var_t* args,
                                    amxc_var_t* ret) {
    amxc_var_set(cstring_t, ret, "1,3"); // 1 = third party, 3 = filter

    return 0;
}

static void modpcp_logger(pcp_loglvl_e lvl, const char* msg) {
    switch(lvl) {
    case PCP_LOGLVL_ERR:
        SAH_TRACEZ_ERROR(ME, LOG_FMT, msg);
        break;
    case PCP_LOGLVL_WARN:
        SAH_TRACEZ_WARNING(ME, LOG_FMT, msg);
        break;
    default:
        SAH_TRACEZ_INFO(ME, LOG_FMT, msg);
    }
}

static int modpcp_debug(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    bool enable = GET_BOOL(args, NULL);

    pcp_log_level = enable ? PCP_LOGLVL_INFO : PCP_LOGLVL_WARN;

    return 0;
}

static AMXM_CONSTRUCTOR module_init(void) {
    int rv = -1;

    amxc_htable_init(&servers, 0);

    rv = amxm_module_register(&mod, amxm_so_get_current(), "pcp");
    when_false(0 == rv, exit);

    pcp_set_loggerfn(modpcp_logger);
    pcp_log_level = PCP_LOGLVL_WARN;

    rv = 0;
    rv |= amxm_module_add_function(mod, "map-add", modpcp_map_add);
    rv |= amxm_module_add_function(mod, "map-remove", modpcp_map_remove);
    rv |= amxm_module_add_function(mod, "server-add", modpcp_server_add);
    rv |= amxm_module_add_function(mod, "server-remove", modpcp_server_remove);
    rv |= amxm_module_add_function(mod, "supported-versions", modpcp_supported_versions);
    rv |= amxm_module_add_function(mod, "supported-options", modpcp_supported_options);
    rv |= amxm_module_add_function(mod, "debug", modpcp_debug);
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Amxm module[libpcp] failed to add functions");
        rv = -1;
    }

exit:
    return rv;
}

static AMXM_DESTRUCTOR module_exit(void) {
    amxc_htable_clean(&servers, server_it_delete);

    return 0;
}
