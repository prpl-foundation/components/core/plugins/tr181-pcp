/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <arpa/inet.h>

#include <ares.h>
#include <amxc/amxc_macros.h>

void ares_getaddrinfo(ares_channel channel,
                      const char* name,
                      UNUSED const char* service,
                      UNUSED const struct ares_addrinfo_hints* hints,
                      UNUSED ares_addrinfo_callback callback,
                      void* arg) {
    struct ares_addrinfo* result = NULL;
    struct ares_addrinfo_node* nodes[] = {NULL, NULL};
    assert_non_null(channel);
    assert_string_equal(name, "aftr.example.com");

    result = calloc(1, sizeof(struct ares_addrinfo));
    nodes[0] = calloc(1, sizeof(struct ares_addrinfo_node));
    nodes[1] = calloc(1, sizeof(struct ares_addrinfo_node));
    assert_non_null(nodes[0]);
    assert_non_null(nodes[1]);

    nodes[0]->ai_family = AF_INET;
    nodes[0]->ai_ttl = 22;
    nodes[0]->ai_addr = calloc(1, sizeof(struct sockaddr_storage));
    assert_non_null(nodes[0]->ai_addr);
    inet_pton(AF_INET, "216.58.201.238", &((struct sockaddr_in*) nodes[0]->ai_addr)->sin_addr);
    nodes[0]->ai_next = nodes[1];

    nodes[1]->ai_family = AF_INET6;
    nodes[1]->ai_ttl = 22;
    nodes[1]->ai_addr = calloc(1, sizeof(struct sockaddr_storage));
    assert_non_null(nodes[1]->ai_addr);
    inet_pton(AF_INET6, "2a00:1450:4007:816::200e", &((struct sockaddr_in6*) nodes[1]->ai_addr)->sin6_addr);

    result->nodes = nodes[0];

    callback(arg, ARES_SUCCESS, 0, result);
    result = NULL; // freed by callback?
}

struct timeval* ares_timeout(ares_channel channel,
                             struct timeval* maxtv,
                             struct timeval* tv) {
    assert_non_null(channel);
    assert_non_null(maxtv);
    assert_non_null(tv);
    return tv;
}

void ares_freeaddrinfo(struct ares_addrinfo* result) {
    assert_non_null(result);

    for(struct ares_addrinfo_node* ai = result->nodes; ai != NULL;) {
        struct ares_addrinfo_node* tmp = NULL;
        tmp = ai;
        ai = ai->ai_next;
        free(tmp->ai_addr);
        free(tmp);
    }
    free(result);
}

void ares_process_fd(UNUSED ares_channel channel,
                     UNUSED ares_socket_t read_fd,
                     UNUSED ares_socket_t write_fd) {
}

int ares_init_options(UNUSED ares_channel* channelptr,
                      UNUSED struct ares_options* options,
                      UNUSED int optmask) {
    *channelptr = calloc(1, 1); // alloc something for Valgrind to track open/close
    assert_non_null(*channelptr);
    return 0;
}

void ares_destroy(ares_channel channel) {
    if(channel != NULL) {
        free(channel);
    }
}

int ares_library_init(UNUSED int flags) {
    return ARES_SUCCESS;
}

void ares_library_cleanup(UNUSED void) {
}

const char* ares_strerror(UNUSED int code) {
    return "";
}