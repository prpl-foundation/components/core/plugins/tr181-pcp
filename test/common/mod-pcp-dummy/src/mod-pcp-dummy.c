/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>

static void call_mod_unit_test(const char* function, amxc_var_t* args) {
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxm_execute_function(NULL, "unit_tests", function, args, &ret);
    amxc_var_clean(&ret);
}

static void enable_map(const char* mid) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    // hardcoded for now
    amxc_var_add_key(cstring_t, &args, "client-id", "client-1");
    amxc_var_add_key(cstring_t, &args, "server-id", "server-1");
    amxc_var_add_key(cstring_t, &args, "map-id", mid != NULL ? mid : "map-1");
    amxc_var_add_key(cstring_t, &args, "status", "Enabled");
    amxc_var_add_key(cstring_t, &args, "external-ip", "0.0.0.0");
    amxc_var_add_key(uint32_t, &args, "external-port", 9000);

    rv = amxm_execute_function(NULL, "core", "map-state-changed", &args, &ret);
    assert_int_equal(rv, 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static int dummy_map_add(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    call_mod_unit_test("pcp_add_map", args);
    enable_map(GETP_CHAR(args, "map.0.id"));
    return 0;
}

static int dummy_map_remove(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    call_mod_unit_test("pcp_remove_map", args);
    return 0;
}

static int dummy_server_add(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    call_mod_unit_test("pcp_add_server", args);
    enable_map(NULL);
    return 0;
}

static int dummy_server_remove(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    call_mod_unit_test("pcp_remove_server", args);
    return 0;
}

static int dummy_supported_versions(UNUSED const char* function_name,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    return 0;
}

static int dummy_supported_options(UNUSED const char* function_name,
                                   UNUSED amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    return 0;
}

static int dummy_not_implemented(UNUSED const char* function_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    assert_int_equal(1, 2); // should not be called
    return 0;
}

typedef struct {
    const char* name;
    amxm_callback_t callback;
} function_t;

static AMXM_CONSTRUCTOR module_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    function_t function[] = {
        {"map-add", dummy_map_add},
        {"map-remove", dummy_map_remove},
        {"server-add", dummy_server_add},
        {"server-remove", dummy_server_remove},
        {"supported-versions", dummy_supported_versions},
        {"supported-options", dummy_supported_options},
        {"debug", dummy_not_implemented},
        {NULL, 0}
    };

    amxm_module_register(&mod, so, "pcp");
    for(size_t i = 0; function[i].name != NULL; i++) {
        amxm_module_add_function(mod, function[i].name, function[i].callback);
    }

    return 0;
}
