/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>

#include "mock.h"
#include "mock_netmodel.h"
#include "dm_pcp.h"
#include "pcp.h"

#define PREFIX "X_PRPL-COM_"

static amxd_dm_t* g_dm = NULL;
static amxo_parser_t* g_parser = NULL;

amxd_dm_t* unit_tests_dm(void) {
    return g_dm;
}

void pcp_unit_test_setup(amxd_dm_t** dm, amxo_parser_t** parser, const char* odl) {
    amxd_object_t* root = NULL;
    amxm_module_t* mod = NULL;
    int retval = 0;
    const char* default_odl =
        "%config {"
        "    include-dirs = ["
        "        \"../../odl/\""
        "    ];"
        "    modules.mod-pcp = \"" MOD_PCP "\";"
        "    modules.mod-firewall = \"../common/modules/mod-fw-dummy.so\";"
        "    prefix_ = \"" PREFIX "\";"
        "}"
        "include \"tr181-pcp_definition.odl\";"
        "%define {"
        "}"
        "%populate {"
        "    object PCP {"
        "        parameter Enable = true;"
        "    }"
        "}";

    if(odl == NULL) {
        odl = default_odl;
    }

    g_dm = amxut_bus_dm();
    g_parser = amxut_bus_parser();
    assert_non_null(g_parser);
    assert_non_null(g_dm);

    root = amxd_dm_get_root(g_dm);
    assert_non_null(root);

    //events
    amxo_resolver_ftab_add(g_parser, "app_start", AMXO_FUNC(_app_start));
    amxo_resolver_ftab_add(g_parser, "debug_changed", AMXO_FUNC(_debug_changed));
    amxo_resolver_ftab_add(g_parser, "client_added", AMXO_FUNC(_client_added));
    amxo_resolver_ftab_add(g_parser, "client_enable_changed", AMXO_FUNC(_client_enable_changed));
    amxo_resolver_ftab_add(g_parser, "client_destroy", AMXO_FUNC(_client_destroy));
    amxo_resolver_ftab_add(g_parser, "server_added", AMXO_FUNC(_server_added));
    amxo_resolver_ftab_add(g_parser, "server_enable_changed", AMXO_FUNC(_server_enable_changed));
    amxo_resolver_ftab_add(g_parser, "server_nameoraddress_changed", AMXO_FUNC(_server_nameoraddress_changed));
    amxo_resolver_ftab_add(g_parser, "server_destroy", AMXO_FUNC(_server_destroy));
    amxo_resolver_ftab_add(g_parser, "inboundmapping_added", AMXO_FUNC(_inboundmapping_added));
    amxo_resolver_ftab_add(g_parser, "inboundmapping_changed", AMXO_FUNC(_inboundmapping_changed));
    amxo_resolver_ftab_add(g_parser, "inboundmapping_destroy", AMXO_FUNC(_inboundmapping_destroy));
    amxo_resolver_ftab_add(g_parser, "lifetime_changed", AMXO_FUNC(_lifetime_changed));
    amxo_resolver_ftab_add(g_parser, "filter_added", AMXO_FUNC(_filter_added));
    amxo_resolver_ftab_add(g_parser, "filter_changed", AMXO_FUNC(_filter_changed));
    amxo_resolver_ftab_add(g_parser, "enable_changed", AMXO_FUNC(_enable_changed));

    // to be able to use cmocka's expect_check
    amxm_module_register(&mod, NULL, "unit_tests");
    amxm_module_add_function(mod, "fw_set_pinhole", unit_tests_set_pinhole);
    amxm_module_add_function(mod, "fw_delete_pinhole", unit_tests_delete_pinhole);
    amxm_module_add_function(mod, "pcp_add_server", unit_tests_add_server);
    amxm_module_add_function(mod, "pcp_remove_server", unit_tests_remove_server);
    amxm_module_add_function(mod, "pcp_add_map", unit_tests_add_map);
    amxm_module_add_function(mod, "pcp_remove_map", unit_tests_remove_map);

    mock_netmodel_init(g_dm, g_parser, root);

    retval = amxo_parser_parse_string(g_parser, odl, root);
    printf("PARSER MESSAGE = %s\n", amxc_string_get(&g_parser->msg, 0));
    assert_int_equal(retval, 0);

    handle_events();

    *dm = g_dm;
    *parser = g_parser;
}

void pcp_unit_test_teardown(void) {
    amxp_sigmngr_emit_signal(&g_dm->sigmngr, "app:stop", NULL);
    handle_events();

    mock_netmodel_clean();

    g_parser = NULL;
    g_dm = NULL;
}
