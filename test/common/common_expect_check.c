/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "mock.h"

static bool compare_amxc_variant(const char* id, const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* a = (amxc_var_t*) value;
    amxc_var_t* b = (amxc_var_t*) check_value_data;
    int result = -1;

    if((amxc_var_compare(a, b, &result) != 0) ||
       (result != 0)) {
        print_message("<%s from plugin>\n", id);
        amxc_var_dump(a, 1);
        print_message("<%s from unit test>\n", id);
        amxc_var_dump(b, 1);
        result = 0;
    } else {
        result = 1;
    }

    amxc_var_delete(&b);
    return result;
}

int pinhole_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    return compare_amxc_variant("pinhole", value, check_value_data);
}

int server_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    return compare_amxc_variant("server", value, check_value_data);
}

int map_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    return compare_amxc_variant("map", value, check_value_data);
}

int unit_tests_set_pinhole(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_delete_pinhole(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_add_server(UNUSED const char* function_name,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_remove_server(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_add_map(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_remove_map(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

amxc_var_t* new_set_pinhole(const char* id, uint32_t port, bool enable, const char* interface, const char* dest, const char* origin, int protocol, uint32_t lease_duration) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "id", id);
    amxc_var_add_key(cstring_t, var, "interface", interface);
    amxc_var_add_key(int32_t, var, "protocol", protocol);
    amxc_var_add_key(bool, var, "enable", enable);
    amxc_var_add_key(uint32_t, var, "destination_port", port);
    amxc_var_add_key(cstring_t, var, "origin", origin);
    amxc_var_add_key(cstring_t, var, "internal_client", dest);
    amxc_var_add_key(uint32_t, var, "lease_duration", lease_duration);
    return var;
}

amxc_var_t* new_delete_pinhole(const char* id) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "id", id);
    return var;
}

amxc_var_t* new_add_server(const char* client, const char* server, const char* ip, uint32_t port, amxc_var_t* map) {
    amxc_var_t* var = NULL;
    amxc_var_t* subvar = NULL;

    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(amxc_llist_t, var, "map", amxc_var_constcast(amxc_llist_t, map));

    subvar = amxc_var_add_key(amxc_htable_t, var, "server", NULL);
    amxc_var_add_key(cstring_t, subvar, "client-id", client);
    amxc_var_add_key(cstring_t, subvar, "id", server);
    amxc_var_add_key(cstring_t, subvar, "ipaddress", ip);
    amxc_var_add_key(uint32_t, subvar, "port", port);
    amxc_var_add_key(uint32_t, subvar, "version", 2);

    return var;
}

amxc_var_t* new_remove_server(const char* id) {
    amxc_var_t* var = NULL;
    amxc_var_t* subvar = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    subvar = amxc_var_add_key(amxc_htable_t, var, "server", NULL);
    amxc_var_add_key(cstring_t, subvar, "id", id);
    return var;
}

amxc_var_t* new_add_map(const char* server, const char* map, const char* src_addr, uint32_t port, int32_t protocol, uint32_t lifetime, const char* third_party_address, amxc_var_t* filter) {
    amxc_var_t* var = NULL;
    amxc_var_t* subvar = NULL;

    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);

    subvar = amxc_var_add_key(amxc_llist_t, var, "map", NULL);
    subvar = amxc_var_add(amxc_htable_t, subvar, NULL);
    amxc_var_add_key(amxc_llist_t, subvar, "filter", amxc_var_constcast(amxc_llist_t, filter));
    amxc_var_add_key(cstring_t, subvar, "id", map);
    amxc_var_add_key(cstring_t, subvar, "srcaddress", src_addr);
    amxc_var_add_key(uint32_t, subvar, "port", port);
    amxc_var_add_key(int32_t, subvar, "protocol", protocol);
    amxc_var_add_key(uint32_t, subvar, "lifetime", lifetime);
    amxc_var_add_key(cstring_t, subvar, "thirdparty_address", third_party_address);

    subvar = amxc_var_add_key(amxc_htable_t, var, "server", NULL);
    amxc_var_add_key(cstring_t, subvar, "id", server);

    return var;
}

amxc_var_t* new_remove_map(const char* server, const char* map) {
    amxc_var_t* var = NULL;
    amxc_var_t* subvar = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    subvar = amxc_var_add_key(amxc_htable_t, var, "server", NULL);
    amxc_var_add_key(cstring_t, subvar, "id", server);
    subvar = amxc_var_add_key(amxc_llist_t, var, "map", NULL);
    subvar = amxc_var_add(amxc_htable_t, subvar, NULL);
    amxc_var_add_key(cstring_t, subvar, "id", map);
    return var;
}