/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __COMMON_MOCK_H__
#define __COMMON_MOCK_H__

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_be.h>
#include <amxb/amxb_connect.h>

void pcp_unit_test_setup(amxd_dm_t** dm, amxo_parser_t** parser, const char* odl);
void pcp_unit_test_teardown(void);
#define handle_events amxut_bus_handle_events
amxd_dm_t* unit_tests_dm(void);

void change_netdevname(const char* tr181_interface, const char* netdevname);
void change_ipv6(const char* tr181_interface, const char* ip);
void change_dhcp_option(const char* tr181_interface, const char* dnsservers);
void change_dnsservers(const char* tr181_interface, const char* dnsservers);

int pinhole_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data);
int server_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data);
int map_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data);

int unit_tests_set_pinhole(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_delete_pinhole(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_add_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_remove_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_add_map(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_remove_map(const char* function_name, amxc_var_t* args, amxc_var_t* ret);

amxc_var_t* new_set_pinhole(const char* id, uint32_t port, bool enable, const char* interface, const char* dest, const char* origin, int protocol, uint32_t lease_duration);
amxc_var_t* new_delete_pinhole(const char* id);
amxc_var_t* new_add_server(const char* client, const char* server, const char* ip, uint32_t port, amxc_var_t* map);
amxc_var_t* new_remove_server(const char* name);
amxc_var_t* new_add_map(const char* server, const char* map, const char* src_addr, uint32_t port, int32_t protocol, uint32_t lifetime, const char* third_party_address, amxc_var_t* filter);
amxc_var_t* new_remove_map(const char* server, const char* map);

#endif // __COMMON_MOCK_H__
