/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "dm_pcp.h"
#include "pcp.h"
#include "test_start_stop.h"
#include "mock.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

#define expect_ut_add_server(CID, SID, IP, PORT, MAP) do { \
        amxc_var_t* __var = new_add_server(CID, SID, IP, PORT, MAP); \
        expect_check(unit_tests_add_server, args, server_equal_check, __var); \
} while(0);

#define expect_ut_remove_server(SID) do { \
        amxc_var_t* __var = new_remove_server(SID); \
        expect_check(unit_tests_remove_server, args, server_equal_check, __var); \
} while(0);

#define expect_ut_add_map(SID, MID, SRC, PORT, PROTOCOL, LIFETIME, DEST, FILTER) do { \
        amxc_var_t* __var = new_add_map(SID, MID, SRC, PORT, PROTOCOL, LIFETIME, DEST, FILTER); \
        expect_check(unit_tests_add_map, args, map_equal_check, __var); \
} while(0);

#define expect_ut_remove_map(SID, MID) do { \
        amxc_var_t* __var = new_remove_map(SID, MID); \
        expect_check(unit_tests_remove_map, args, map_equal_check, __var); \
} while(0);

#define expect_ut_set_pinhole(ID, PORT, ADDR, PROTOCOL, DURATION) do { \
        amxc_var_t* __var = new_set_pinhole(ID, PORT, true, "Device.IP.Interface.2.", ADDR, "System", PROTOCOL, DURATION); \
        expect_check(unit_tests_set_pinhole, args, pinhole_equal_check, __var); \
} while(0);

#define expect_ut_delete_pinhole(ID) do { \
        amxc_var_t* __var = new_delete_pinhole(ID); \
        expect_check(unit_tests_delete_pinhole, args, pinhole_equal_check, __var); \
} while(0);

int test_setup(void** state) {
    amxut_bus_setup(state);
    pcp_unit_test_setup(&dm, &parser, NULL);

    assert_int_equal(_pcp_main(0, dm, parser), 0);

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);
    handle_events();

    return 0;
}

int test_teardown(void** state) {
    assert_int_equal(_pcp_main(1, dm, parser), 0);

    pcp_unit_test_teardown();
    amxut_bus_teardown(state);

    return 0;
}

void test_add_client(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* client = amxd_dm_findf(dm, "PCP.Client.client-1.");

    assert_null(client);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client");
    amxd_trans_add_inst(&trans, 0, "client-1");
    amxd_trans_set_value(cstring_t, &trans, "WANInterface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.", "Status", "Enabled");
}

void test_add_server(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* server = NULL;

    server = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.");
    assert_null(server);

    print_message("add 'PCP.Client.client-1.Server.server-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server");
    amxd_trans_add_inst(&trans, 0, "server-1");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "DHCPv6");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_server("client-1", "server-1", "2a00:1450:4007:816::200e", 5351, NULL);
    expect_ut_add_map("server-1", "dummy", "2a02::1", 9, 17, 5, "192.168.1.1", NULL);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.", "ServerAddressInUse", "2a00:1450:4007:816::200e");
    amxut_dm_param_equals(csv_string_t, "PCP.Client.client-1.Server.server-1.", "AdditionalServerAddresses", "");
}

void test_add_inboundmapping(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* map = NULL;

    map = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.");
    assert_null(map);

    print_message("add 'PCP.Client.client-1.Server.server-1.InboundMapping.map-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping");
    amxd_trans_add_inst(&trans, 0, "map-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", 6);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 9000);
    amxd_trans_set_value(cstring_t, &trans, "ThirdPartyAddress", "192.168.0.228");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-1", "2a02::1", 9000, 6, 0, "192.168.0.228", NULL);
    expect_ut_set_pinhole("map-1", 9000, "192.168.0.228", 6, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");
}

void test_inmap_change_protocol(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1");
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", 17);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-1", "2a02::1", 9000, 17, 0, "192.168.0.228", NULL);
    expect_ut_set_pinhole("map-1", 9000, "192.168.0.228", 17, 0);
    handle_events();
}

void test_add_filter(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* map = NULL;
    amxc_var_t filter;
    amxc_var_t* f1 = NULL;

    map = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.");
    assert_non_null(map);

    amxc_var_init(&filter);
    amxc_var_set_type(&filter, AMXC_VAR_ID_LIST);
    f1 = amxc_var_add(amxc_htable_t, &filter, NULL);
    amxc_var_add_key(cstring_t, f1, "ipaddress", "");
    amxc_var_add_key(uint32_t, f1, "port", 0);
    amxc_var_add_key(uint32_t, f1, "prefix", 128);

    print_message("add 'PCP.Client.client-1.Server.server-1.InboundMapping.map-1.Filter.filter-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.Filter");
    amxd_trans_add_inst(&trans, 0, "filter-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-1", "2a02::1", 9000, 17, 0, "192.168.0.228", &filter);
    expect_ut_set_pinhole("map-1", 9000, "192.168.0.228", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");
    amxc_var_clean(&filter);
}

void test_change_filter(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* map = NULL;
    amxc_var_t filter;
    amxc_var_t* f1 = NULL;

    map = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.");
    assert_non_null(map);

    amxc_var_init(&filter);
    amxc_var_set_type(&filter, AMXC_VAR_ID_LIST);
    f1 = amxc_var_add(amxc_htable_t, &filter, NULL);
    amxc_var_add_key(cstring_t, f1, "ipaddress", "");
    amxc_var_add_key(uint32_t, f1, "port", 0);
    amxc_var_add_key(uint32_t, f1, "prefix", 24);

    print_message("change parameter PrefixLength of 'PCP.Client.client-1.Server.server-1.InboundMapping.map-1.Filter.filter-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.Filter.filter-1");
    amxd_trans_set_value(uint32_t, &trans, "PrefixLength", 24);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-1", "2a02::1", 9000, 17, 0, "192.168.0.228", &filter);
    expect_ut_set_pinhole("map-1", 9000, "192.168.0.228", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");

    amxc_var_clean(&filter);
}

void test_change_inboundmapping(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* map = NULL;
    amxc_var_t filter;
    amxc_var_t* f1 = NULL;

    map = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.");
    assert_non_null(map);

    amxc_var_init(&filter);
    amxc_var_set_type(&filter, AMXC_VAR_ID_LIST);
    f1 = amxc_var_add(amxc_htable_t, &filter, NULL);
    amxc_var_add_key(cstring_t, f1, "ipaddress", "");
    amxc_var_add_key(uint32_t, f1, "port", 0);
    amxc_var_add_key(uint32_t, f1, "prefix", 24);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1");
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 6000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-1", "2a02::1", 6000, 17, 0, "192.168.0.228", &filter);
    expect_ut_set_pinhole("map-1", 6000, "192.168.0.228", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");

    amxc_var_clean(&filter);
}

void test_change_server(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* map = NULL;
    amxc_var_t* m1 = NULL;
    amxc_var_t filter;
    amxc_var_t* f1 = NULL;

    map = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.");
    assert_non_null(map);

    amxc_var_init(&filter);
    amxc_var_set_type(&filter, AMXC_VAR_ID_LIST);
    f1 = amxc_var_add(amxc_htable_t, &filter, NULL);
    amxc_var_add_key(cstring_t, f1, "ipaddress", "");
    amxc_var_add_key(uint32_t, f1, "port", 0);
    amxc_var_add_key(uint32_t, f1, "prefix", 24);
    m1 = new_add_map("server-1", "map-1", "2a02::1", 6000, 17, 0, "192.168.0.228", &filter); // hack to reuse new_add_map for new_add_server

    print_message("disable 'PCP.Client.client-1.Server.server-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_remove_map("server-1", "dummy");
    expect_ut_remove_server("server-1");
    expect_ut_delete_pinhole("map-1");
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Disabled");

    print_message("enable 'PCP.Client.client-1.Server.server-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_server("client-1", "server-1", "2a00:1450:4007:816::200e", 5351, GET_ARG(m1, "map"));
    expect_ut_add_map("server-1", "dummy", "2a02::1", 9, 17, 5, "192.168.1.1", NULL);
    expect_ut_set_pinhole("map-1", 6000, "192.168.0.228", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");

    amxc_var_clean(&filter);
    amxc_var_delete(&m1);
}

void test_change_client(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* map = NULL;
    amxc_var_t* m1 = NULL;
    amxc_var_t filter;
    amxc_var_t* f1 = NULL;

    map = amxd_dm_findf(dm, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1.");
    assert_non_null(map);

    amxc_var_init(&filter);
    amxc_var_set_type(&filter, AMXC_VAR_ID_LIST);
    f1 = amxc_var_add(amxc_htable_t, &filter, NULL);
    amxc_var_add_key(cstring_t, f1, "ipaddress", "");
    amxc_var_add_key(uint32_t, f1, "port", 0);
    amxc_var_add_key(uint32_t, f1, "prefix", 24);
    m1 = new_add_map("server-1", "map-1", "2a02::1", 6000, 17, 0, "192.168.0.228", &filter); // hack to reuse new_add_map for new_add_server

    print_message("disable 'PCP.Client.client-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_remove_map("server-1", "dummy");
    expect_ut_remove_server("server-1");
    expect_ut_delete_pinhole("map-1");
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Disabled");

    print_message("enable 'PCP.Client.client-1'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_server("client-1", "server-1", "2a00:1450:4007:816::200e", 5351, GET_ARG(m1, "map"));
    expect_ut_add_map("server-1", "dummy", "2a02::1", 9, 17, 5, "192.168.1.1", NULL);
    expect_ut_set_pinhole("map-1", 6000, "192.168.0.228", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");

    amxc_var_delete(&m1);
    amxc_var_clean(&filter);
}

void test_lifetime_expires(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping");
    amxd_trans_add_inst(&trans, 0, "map-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", 17);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 7000);
    amxd_trans_set_value(uint32_t, &trans, "Lifetime", 0);
    amxd_trans_set_value(cstring_t, &trans, "ThirdPartyAddress", "192.168.1.100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_remove_map("server-1", "dummy");
    expect_ut_add_map("server-1", "map-99", "2a02::1", 7000, 17, 0, "192.168.1.100", NULL);
    expect_ut_set_pinhole("map-99", 7000, "192.168.1.100", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-99", "Status", "Enabled");

    amxut_timer_go_to_future_ms(60000); // to show lifetime 0 is static so never removed

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-99");
    amxd_trans_set_value(uint32_t, &trans, "Lifetime", 10);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_set_pinhole("map-99", 7000, "192.168.1.100", 17, 10);
    handle_events();

    amxut_timer_go_to_future_ms(9999);

    expect_ut_remove_map("server-1", "map-99");
    expect_ut_delete_pinhole("map-99");
    amxut_timer_go_to_future_ms(2); // at this point Lifetime expired (10 sec < 9999 ms + 2 ms)
}

void test_inmap_disabled_doesnt_add_map(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t* m1 = NULL;
    amxc_var_t filter;
    amxc_var_t* f1 = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping");
    amxd_trans_add_inst(&trans, 0, "map-100");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", 17);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 7000);
    amxd_trans_set_value(uint32_t, &trans, "Lifetime", 0);
    amxd_trans_set_value(cstring_t, &trans, "ThirdPartyAddress", "192.168.1.100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100", "Status", "Disabled");

    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-100", "2a02::1", 7000, 17, 0, "192.168.1.100", NULL);
    expect_ut_set_pinhole("map-100", 7000, "192.168.1.100", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_remove_map("server-1", "map-100");
    expect_ut_delete_pinhole("map-100");
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100", "Status", "Disabled");
    amxut_dm_param_equals(uint32_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100", "AssignedExternalPort", 0);
    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-100", "AssignedExternalIPAddress", "");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping");
    amxd_trans_del_inst(&trans, 0, "map-100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_delete_pinhole("map-1");
    expect_ut_remove_server("server-1");
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Disabled");
    amxut_dm_param_equals(uint32_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "AssignedExternalPort", 0);
    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "AssignedExternalIPAddress", "");

    amxc_var_init(&filter);
    amxc_var_set_type(&filter, AMXC_VAR_ID_LIST);
    f1 = amxc_var_add(amxc_htable_t, &filter, NULL);
    amxc_var_add_key(cstring_t, f1, "ipaddress", "");
    amxc_var_add_key(uint32_t, f1, "port", 0);
    amxc_var_add_key(uint32_t, f1, "prefix", 24);
    m1 = new_add_map("server-1", "map-1", "2a02::1", 6000, 17, 0, "192.168.0.228", &filter); // hack to reuse new_add_map for new_add_server

    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_server("client-1", "server-1", "2a00:1450:4007:816::200e", 5351, GET_ARG(m1, "map"));
    expect_ut_add_map("server-1", "dummy", "2a02::1", 9, 17, 5, "192.168.1.1", NULL);
    expect_ut_set_pinhole("map-1", 6000, "192.168.0.228", 17, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-1", "Status", "Enabled");

    amxc_var_delete(&m1);
    amxc_var_clean(&filter);
}

void test_inmap_all_protocols(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping");
    amxd_trans_add_inst(&trans, 0, "map-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", -1);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 7000); // configure something > 1
    amxd_trans_set_value(uint32_t, &trans, "Lifetime", 0);
    amxd_trans_set_value(cstring_t, &trans, "ThirdPartyAddress", "192.168.1.100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-99", "2a02::1", 0, 0, 0, "192.168.1.100", NULL); // check that protocol and internal port are zero
    expect_ut_set_pinhole("map-99", -1, "192.168.1.100", -1, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping.map-99");
    amxd_trans_set_value(int32_t, &trans, "ProtocolNumber", 6);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_ut_add_map("server-1", "map-99", "2a02::1", 7000, 6, 0, "192.168.1.100", NULL);
    expect_ut_set_pinhole("map-99", 7000, "192.168.1.100", 6, 0);
    handle_events();

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-1.InboundMapping.map-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-1.InboundMapping");
    amxd_trans_del_inst(&trans, 0, "map-99");
    expect_ut_remove_map("server-1", "map-99");
    expect_ut_delete_pinhole("map-99");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_remove_server(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("remove instance 'PCP.Client.client-1.Server.server-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.");
    amxd_trans_del_inst(&trans, 0, "server-1");
    expect_ut_remove_map("server-1", "map-1");
    expect_ut_remove_map("server-1", "dummy");
    expect_ut_delete_pinhole("map-1");
    expect_ut_remove_server("server-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_add_disabled_server_and_then_enable_it(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("add instance of 'PCP.Client.client-1.Server.' with parameter 'Enable == false'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.");
    amxd_trans_add_inst(&trans, 0, "server-2");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "DHCPv6");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change parameter 'PCP.Client.client-1.Server.server-2.Enable' to true\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.client-1.Server.server-2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_ut_add_server("client-1", "server-2", "2a00:1450:4007:816::200e", 5351, NULL);
    expect_ut_add_map("server-2", "dummy", "2a02::1", 9, 17, 5, "192.168.1.1", NULL);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_enable_and_disable_pcp(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* client = NULL;

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.", "Status", "Enabled");

    client = amxd_dm_findf(dm, "PCP.Client.client-2.");
    assert_null(client);

    print_message("add second PCP client instance\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client");
    amxd_trans_add_inst(&trans, 0, "client-2");
    amxd_trans_set_value(cstring_t, &trans, "WANInterface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "PCP.Client.client-2.", "Status", "Disabled");

    print_message("disable PCP\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_ut_remove_map("server-2", "dummy");
    expect_ut_remove_server("server-2");
    handle_events();
    amxd_trans_clean(&trans);

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.", "Status", "StackDisabled");
    amxut_dm_param_equals(cstring_t, "PCP.Client.client-2.", "Status", "Disabled");

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.Server.server-2.", "Status", "Disabled");

    print_message("enable PCP\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_ut_add_server("client-1", "server-2", "2a00:1450:4007:816::200e", 5351, NULL);
    expect_ut_add_map("server-2", "dummy", "2a02::1", 9, 17, 5, "192.168.1.1", NULL);
    handle_events();
    amxd_trans_clean(&trans);

    amxut_dm_param_equals(cstring_t, "PCP.Client.client-1.", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "PCP.Client.client-2.", "Status", "Disabled");

    print_message("remove client-2\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "PCP.Client.");
    amxd_trans_del_inst(&trans, 0, "client-2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}
