# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.10.11 - 2024-11-18(12:39:06 +0000)

### Other

- - [UPnP-IGD] PCP interworking function is working even if Device.PCP.Client.1.UPnPIWF.Enable=0

## Release v0.10.10 - 2024-10-10(09:27:12 +0000)

### Other

- - [PCP] The DUT sends a PCP MAP request using an LLA address instead of a GUA

## Release v0.10.9 - 2024-09-26(09:19:00 +0000)

### Other

- tr181 v2.17 compliance PCP non-breaking changes

## Release v0.10.8 - 2024-09-23(11:21:23 +0000)

### Other

- - [PCP] Default Lifetime and Assigned Lifetime Handling

## Release v0.10.7 - 2024-09-19(13:48:01 +0000)

### Other

- [tr181-pcp] PCP ExternalIPAddress implementation

## Release v0.10.6 - 2024-08-27(14:24:37 +0000)

### Other

- regression leaves firewall open after reboot

## Release v0.10.5 - 2024-08-19(14:47:52 +0000)

### Other

- support ProtocolNumber -1 (all protocols a.k.a. DMZ host)

## Release v0.10.4 - 2024-08-16(11:14:31 +0000)

### Other

- MAP request sent for disabled InboundMapping

## Release v0.10.3 - 2024-08-13(12:20:13 +0000)

### Other

- InboundMapping not removed after lifetime expires

## Release v0.10.2 - 2024-08-09(12:46:25 +0000)

### Other

- Remote Create rule not correctly configured under Firewall

## Release v0.10.1 - 2024-07-23(07:56:42 +0000)

### Fixes

- Better shutdown script

## Release v0.10.0 - 2024-07-09(12:53:58 +0000)

### New

- amx plugin should not run as root user

## Release v0.9.2 - 2024-05-02(14:36:02 +0000)

### Fixes

- [tr181-pcp] Make it compile with openwrt23.05 toolchain

## Release v0.9.1 - 2024-04-10(16:01:43 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.9.0 - 2024-02-05(16:20:50 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.8.0 - 2024-01-17(09:32:15 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.7.2 - 2023-11-16(14:48:10 +0000)

### Fixes

- [tr181-pcp] ServerAddressInUse and ServerNameOrAddress are not configured

## Release v0.7.1 - 2023-11-06(11:38:06 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v0.7.0 - 2023-10-16(10:27:18 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.6.6 - 2023-10-13(13:47:18 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.6.5 - 2023-09-01(09:49:21 +0000)

### Fixes

- [tr181-pcp] Plugin in error state at boot

## Release v0.6.4 - 2023-08-10(17:50:48 +0000)

### Fixes

- Device.PCP.Enable StackDisabled

## Release v0.6.3 - 2023-08-10(08:41:57 +0000)

### Changes

- use libamxut + split source file

## Release v0.6.2 - 2023-06-06(09:30:05 +0000)

### Fixes

- traffic not forwarded to the third party ipv4 address

## Release v0.6.1 - 2023-05-16(10:17:53 +0000)

### Fixes

- traffic not forwarded to the third party ipv4 address

## Release v0.6.0 - 2023-03-23(15:23:20 +0000)

### Other

- Use sah_libc-ares instead of opensource_c-ares

## Release v0.5.0 - 2023-03-22(08:32:28 +0000)

### New

- Implement DNS retry mechanism

## Release v0.4.1 - 2023-03-09(09:17:38 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.4.0 - 2023-01-31(11:47:57 +0000)

### New

- Add global Enable parameter to PCP datamodel

## Release v0.3.5 - 2023-01-27(11:12:23 +0000)

### Fixes

- Sometimes PCP server stays disabled even though it is enabled

## Release v0.3.4 - 2023-01-12(10:49:34 +0000)

### Fixes

- can't enable server when the server instance was added with Enable==False

## Release v0.3.3 - 2022-12-13(10:08:22 +0000)

### Fixes

- Add option to compile with libraries installed at /opt/prplos

## Release v0.3.2 - 2022-12-09(09:30:56 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.3.1 - 2022-11-22(13:31:28 +0000)

### Other

- Do not build debian packages
- Opensource component

## Release v0.3.0 - 2022-11-15(12:49:04 +0000)

### New

- add firewall rules to forward traffic for PCP mapped packets

## Release v0.2.5 - 2022-10-24(12:14:16 +0000)

### Fixes

- sometimes aftr_name doesn't resolve to ipv6 address at boot

## Release v0.2.4 - 2022-10-24(07:59:35 +0000)

### Fixes

- c-ares timer expired loop

## Release v0.2.3 - 2022-10-14(09:00:09 +0000)

### Changes

- document what is supported

## Release v0.2.2 - 2022-10-13(15:07:52 +0000)

### Fixes

- use libc-ares for async dns

## Release v0.2.1 - 2022-10-13(08:32:57 +0000)

### Other

- Improve plugin boot order

## Release v0.2.0 - 2022-10-07(07:15:36 +0000)

### New

- Implement pcp mapping according to tr181

## Release v0.1.0 - 2022-09-12(06:09:36 +0000)

### New

- Implement pcp mapping according to tr181

