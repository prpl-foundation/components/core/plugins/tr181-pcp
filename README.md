tr181-pcp
=========

install libpcp
------------------
```
git clone <libpcp-git-url>
./autogen.sh
./configure --enable-debug --prefix="/usr"
make
sudo make install
```

start libpcp's pcp-server
-------------------------
```
.pcp_server/pcp-server
#./pcp_app/pcp -i :1234
```

start tr181-pcp (for now server's <address:port> is hardcoded)
--------------------------------------------------------------
```
sudo ubusd &
[sudo netmodel -D]
[sudo netmodel-clients -D]
[sudo ip-manager -D]
[sudo ethernet-manager -D]
[sudo netdev-plugin -D]
[sudo tr181-bridging -D]
sudo tr181-pcp
```

send MAP request
----------------
```
# enable PCP.Client.1.Server.1.InboundMapping.1.
# toggle PCP.Client.1.Enable
```

docker container amxdev
-----------------------

You need to set the default route for the dns resolver to avoid "Could not contact DNS servers" (c-ares error 11) when using fqdn as ServerAddressOrName.
```
sudo ip r a default via 172.17.0.1
```