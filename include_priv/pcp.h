/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__PCP_H__)
#define __PCP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <ares.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/common_api.h>

#define string_empty(X) ((X == NULL) || (*X == '\0'))

#define FREE(X) do { \
        free(X); \
        X = NULL; \
} while(0);

#define CLOSE_QUERY(X) do { \
        if(X != NULL) { \
            netmodel_closeQuery(X); \
            X = NULL; \
        } \
} while(0);

#define GET_OBJECT_PP(X) amxd_object_findf(X, ".^.^");
#define get_client_by_server GET_OBJECT_PP
#define get_server_by_mapping GET_OBJECT_PP
#define get_mapping_by_filter GET_OBJECT_PP
#define DUMMY_NAME "dummy"

typedef enum {
    DISABLED = 0,
    STACKDISABLED,
    SUCCESS,
    ERROR,
    LAST
} pcpstatus_t;

typedef enum {
    UNKNOWN = 0,
    DHCPV6,
    STATIC,
} origin_t;

typedef struct {
    char* netdevname;                   // netdev name
    char* srcaddress;                   // interface GUA
    char* nameoraddress;                // dhcpv6 option aftr
    bool ready;                         // reached a state that it can enable servers
    netmodel_query_t* netdevname_query; // retrieve netdev name
    netmodel_query_t* addr_query;       // retrieve interface GUA
    netmodel_query_t* dhcpv6_query;     // retrieve dhcpv6 option aftr
    amxd_object_t* object;
} client_t;

typedef struct {
    struct {
        struct ares_addrinfo_hints hints;
        struct ares_options options;
        ares_channel channel;
        amxc_var_t* fds;
        amxp_timer_t* getaddrinfo;
        amxp_timer_t* retransmission;
        amxc_var_t* addrinfo;
        amxc_var_t intervals;
        amxc_var_t* interval;
        int optmask;
    } async_dns;
    origin_t origin;
    amxd_object_t* object;
    char* domain_name;
    bool added;
} server_t;

typedef struct {
    bool firewall_opened;
    amxp_timer_t* timer; // for lifetime
    amxd_object_t* obj;
} inmap_t;

static inline const char* name(amxd_object_t* obj) {
    return amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
}

static inline const char* object_da_string(amxd_object_t* object, const char* name) {
    return GET_CHAR(amxd_object_get_param_value(object, name), NULL);
}

amxd_dm_t* get_dm(void);
amxo_parser_t* get_parser(void);
const char* pcpstatus_to_str(pcpstatus_t pcpstatus);
void set_status(amxd_object_t* obj, pcpstatus_t status);
bool stack_is_enabled(void);

void nslookup(server_t* server, const char* nameoraddress);
void nslookup_cb(server_t* server);
void nslookup_cancel(server_t* server);

void get_port_and_protocol(amxd_object_t* obj, int* port, int* protocol, int wildcard_value);

bool open_or_update_firewall(inmap_t* map);
bool close_firewall(inmap_t* map);

client_t* new_client(amxd_object_t* obj);
void all_clients_enable_changed(amxd_object_t* obj, bool enable);
const char* client_get_nameoraddress(amxd_object_t* obj);
const char* client_get_srcaddress(amxd_object_t* obj);
bool client_is_ready(amxd_object_t* obj);

server_t* new_server(amxd_object_t* obj);
void all_servers_set_nameoraddress(amxd_object_t* obj, const char* nameoraddress);
int disable_server(amxd_object_t* templ, amxd_object_t* obj, void* priv);
int enable_server(amxd_object_t* templ, amxd_object_t* obj, void* priv);
bool server_is_ready(amxd_object_t* obj);

inmap_t* new_inboundmapping(amxd_object_t* obj);
int disable_map(amxd_object_t* templ, amxd_object_t* obj, void* priv);
int append_map(amxd_object_t* templ, amxd_object_t* instance, void* priv);
int inboundmapping_state_changed(const char* function_name, amxc_var_t* args, amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif // __PCP_H__
